#ifndef TOKENIZADORCLASE_H
#define TOKENIZADORCLASE_H
#include <iostream>
using namespace std;

class TokenizadorClase {
    friend ostream& operator<<(ostream&, const TokenizadorClase&);
    // cout << "DELIMITADORES: " << delimiters;
public: 
    TokenizadorClase (const TokenizadorClase&);
    
    TokenizadorClase (const string& );
    // Inicializa variable privada delimiters a delimitadoresPalabra
    
    TokenizadorClase();
    // Inicializa delimiters =",;:.-/+*\\'\"{}[]()<>�!�?&#=\t\n\r@"
    ~TokenizadorClase (); //Pone delimiters=""
    
    void copia(const TokenizadorClase&);
    
    TokenizadorClase& operator= (const TokenizadorClase&);
    
    void Tokenizar (const string& , list<string>& ) const;
    // Tokeniza str devolviendo el resultado en tokens. La lista tokens se vaciar� antes de almacenar el resultado de la tokenizaci�n.
    
    bool Tokenizar (const string& , const string& )const;
    /*Tokeniza el fichero i guardando la salida en el fichero f
    (una palabra en cada linea del fichero). Devolver� true si se realiza la tokenizacion
     * de forma correcta enviando a cerr el mensaje correspondiente (p.ej que no exista el archivo i)*/
    
    bool Tokenizar (const string & ) const;
   /*Tokeniza el fichero i guardando la salida en un fichero de nombre i
     a�adiendole la extension .tk (sin eliminar previamente la extension de i por ejemplo,
     * del archivo pp.text se generaria el resultado en pp.txt.tk),
     y que contendr� una palabra en cada linea del fichero. Devlver� true si
     se realiza la tokenizacion de forma correcta enviando a cerr el mensaje 
     * correspondiente (p.ej. que no exista el archivo i)*/
    
    bool TokenizarListaFicheros (const string& ) const;
    /* Tokeniza el fichero i que contiene un nombre de fichero por l�nea 
     guardando la salida en ficheros (uno por cada linea de i) cuyo nombre
     sera el leido en i a�adiendole la extension .tk, y que contendra una 
     * palabra en cada linea del fichero leido en i.Devolvera true si se 
     * realiza la tokenizacion de forma correcta de todos los archivos que
     contiene i: devolvera false en caso contrario enviando acerr el mensaje
     correspondiente (p.ej. que no exista el archivo i, o bien enviando a
     * "cerr" los archivos de i que no existan, luego no se ha de interrumpir
     * la ejecucion si hay algun archivo en i que no exista)*/
    bool TokenizarDirectorio (const string& ) const;
    /* Tokeniza todos los archivos que contenga el directorio i, incluyendo
     los de los subdirectorios, guardando la salida en ficheros cuyo nombre
     sera el de entrada a�adiendole la extension .tk, y que contendra una
     palabra en cada linea del fichero. Devolvera true si se realiza la 
     tokenizacion de forma correcta de toos los archivos; devolvera false en
     caso contrario enviando a cerr el mensaje correspondiente (p.ej. que no
     * exista el directorio i, o los ficheros que no se hayan podido tokenizar)*/
    
    bool DelimitadoresPalabra(const string& );
    // Cambia delimiters por "nuevoDelimiters"
    
    void AnyadirDelimitadoresPalabra(const string& );
    /*A�ade al final de delimiters los nuevos delimitadores que aparezcan
     *  en "nuevoDelimiters" (no se almacenaran caracteres repetidos)*/

    string DelimitadoresPalabra() const;
    //devuelve "Delimiters"
    
private:
    string delimiters;
    /*Delimitadores de terminos. Aunque se modifique la forma de
     almacenamiento interna para mejorar la eficiencia, este campo debe
     * permanecer para indicar el orden en que se introdujeron los
     * delimitadores*/

};

#endif