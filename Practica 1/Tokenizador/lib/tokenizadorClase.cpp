#include <iostream>
#include <list>
#include<algorithm>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdlib>

using namespace std;
#include "../include/tokenizadorClase.h"
    
    ostream& operator<<(ostream &s, const TokenizadorClase &obj){
    s << "DELIMITADORES: " << obj.delimiters;
    }
    // cout << "DELIMITADORES: " << delimiters;
 
    TokenizadorClase::TokenizadorClase (const TokenizadorClase& tokenObj){
        copia(tokenObj);
    }
    
    TokenizadorClase::TokenizadorClase (const string& delimitadoresPalabra){
        delimiters = delimitadoresPalabra;
    }
    // Inicializa variable privada delimiters a delimitadoresPalabra
    
    TokenizadorClase::TokenizadorClase(){
        delimiters = ",;:.-/+*\\ '\"{}[]()<>¡!¿?&#=\t\n\r@";
    }
    // Inicializa delimiters =",;:.-/+*\\ '\"{}[]()<>¡!¿?&#=\t\n\r@"
    TokenizadorClase::~TokenizadorClase (){
        this->delimiters = "";

    } //Pone delimiters=""
    void TokenizadorClase::copia(const TokenizadorClase& tokenObj){
        delimiters = tokenObj.delimiters;
    }
    TokenizadorClase& TokenizadorClase::operator= (const TokenizadorClase &tokenObj){
        if(this != &tokenObj){
            (*this).~TokenizadorClase();
            copia(tokenObj);
        }
        return *this;
    }
    
    void TokenizadorClase::Tokenizar (const string& str, list<string>& tokens) const{
        string::size_type lastPos = str.find_first_not_of(delimiters, 0);
        string::size_type pos = str.find_first_of(delimiters,lastPos);
        
        while(string::npos != pos || string::npos != lastPos){
            tokens.push_back(str.substr(lastPos, pos - lastPos));
            lastPos = str.find_first_not_of(delimiters, pos);
            pos = str.find_first_of(delimiters, lastPos);
        }
    }
    /* Tokeniza str devolviendo el resultado en tokens. La lista tokens se vaciará
    antes de almacenar el resultado de la tokenización.*/
    
    bool TokenizadorClase::Tokenizar (const string& fichEntr, const string& fichSal)const{
        ifstream i;
        ofstream f;
        string cadena;
        list<string> tokens;
        
        i.open(fichEntr.c_str());
        if(!i){
            cerr << "ERROR: No existe el archivo: " << fichEntr << endl;
            return false;
        }else{
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    Tokenizar(cadena,tokens);
                }
            }
        }
        i.close();
        f.open(fichSal.c_str());
        list<string>::iterator itS;
        for(itS = tokens.begin(); itS!= tokens.end(); itS++){
            f << (*itS) << endl;
        }
        f.close();
        return true;
        
    }
    /*Tokeniza el fichero i guardando la salida en el fichero f
    (una palabra en cada linea del fichero). Devolverá true si se realiza la tokenizacion
     * de forma correcta enviando a cerr el mensaje correspondiente (p.ej que no exista el archivo i)*/
    
   bool TokenizadorClase::Tokenizar (const string & i) const{
       string f = i + ".tk";
       return Tokenizar(i,f);
   }
   /*Tokeniza el fichero i guardando la salida en un fichero de nombre i
     añadiendole la extension .tk (sin eliminar previamente la extension de i por ejemplo,
     * del archivo pp.text se generaria el resultado en pp.txt.tk),
     y que contendrá una palabra en cada linea del fichero. Devlverá true si
     se realiza la tokenizacion de forma correcta enviando a cerr el mensaje 
     * correspondiente (p.ej. que no exista el archivo i)*/
    
    bool TokenizadorClase::TokenizarListaFicheros(const string& i) const{
        string line;
        bool allExist = true;
        ifstream ifs;
        ifs.open(i.c_str());
        while (getline(ifs, line)) {
            if(!Tokenizar(line)){
                allExist = false;
            }
        }
        ifs.close();
        return allExist;
    }
    /* Tokeniza el fichero i que contiene un nombre de fichero por línea 
     guardando la salida en ficheros (uno por cada linea de i) cuyo nombre
     sera el leido en i añadiendole la extension .tk, y que contendra una 
     * palabra en cada linea del fichero leido en i.Devolvera true si se 
     * realiza la tokenizacion de forma correcta de todos los archivos que
     contiene i: devolvera false en caso contrario enviando acerr el mensaje
     correspon
     * diente (p.ej. que no exista el archivo i, o bien enviando a
     * "cerr" los archivos de i que no existan, luego no se ha de interrumpir
     * la ejecucion si hay algun archivo en i que no exista)*/
    
    bool TokenizadorClase::TokenizarDirectorio (const string& i) const{
        struct stat dir;
        
        int err=stat(i.c_str(), &dir);
        if(err == -1 ||!S_ISDIR(dir.st_mode)){
            return false;
        }else{
            string cmd = "find "+i+" -follow |sort > .lista_fich";
            system(cmd.c_str());
            return TokenizarListaFicheros(".lista_fich");
        }       
    }
    /* Tokeniza todos los archivos que contenga el directorio i, incluyendo
     los de los subdirectorios, guardando la salida en ficheros cuyo nombre
     sera el de entrada añadiendole la extension .tk, y que contendra una
     palabra en cada linea del fichero. Devolvera true si se realiza la 
     tokenizacion de forma correcta de toos los archivos; devolvera false en
     caso contrario enviando a cerr el mensaje correspondiente (p.ej. que no
     * exista el directorio i, o los ficheros que no se hayan podido tokenizar)*/
    
    bool TokenizadorClase::DelimitadoresPalabra(const string& nuevoDelimiters){
        delimiters = nuevoDelimiters;
    }
    // Cambia delimiters por "nuevoDelimiters"
    
    void TokenizadorClase::AnyadirDelimitadoresPalabra(const string& nuevoDelimiters){
        string str = nuevoDelimiters;
        for (int i = 0; i < str.size(); i++){
            
            if (delimiters.find(str.substr(i,1)) == string::npos){
                delimiters += str.at(i);
            }  
        }
    }
    /*Añade al final de delimiters los nuevos delimitadores que aparezcan
     *  en "nuevoDelimiters" (no se almacenaran caracteres repetidos)*/

    string TokenizadorClase::DelimitadoresPalabra() const{
        return delimiters;
    }
    //devuelve "Delimiters"
    
