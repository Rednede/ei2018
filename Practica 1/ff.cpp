if(delim == ':'){


                charsBeforeDelim = delimPos - firstPos;
                //if there is a char that is not a delimiters without url ones, then it can be a url
                
                if(delimitersWithoutURL.find(str.at(delimPos + 1)) == string::npos && (
                        (str.substr(firstPos, charsBeforeDelim ) == "ftp")|| 
                        (str.substr(firstPos, charsBeforeDelim ) == "http") || 
                        (str.substr(firstPos, charsBeforeDelim ) == "https"))){

                    delimPos = str.find_first_of(delimitersWithoutURL,delimPos);
                    tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                    processed = true;
                }else{
                    //the string before the : is not part of an url and : is a delimiter
                    tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                    processed = true;
                }         
            }
