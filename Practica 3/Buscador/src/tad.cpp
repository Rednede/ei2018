#include <iostream> 
#include <string>
#include <list> 
#include <sys/resource.h>
#include "tokenizador.h"
#include "indexadorHash.h"
#include "buscador.h"
#include <fstream>

using namespace std;

double getcputime(void) {
   struct timeval tim;
   struct rusage ru;
   getrusage(RUSAGE_SELF, &ru);
   tim = ru.ru_utime;
   double t = (double) tim.tv_sec + (double) tim.tv_usec / 1000000.0;
   tim = ru.ru_stime;
   t += (double) tim.tv_sec + (double) tim.tv_usec / 1000000.0;
   return t;
}

int main(void) {
   /*
    string nombre_doc = "./corpus";
    long double aa;

    cout << "Indice Normal" << endl;
    aa = getcputime();
    IndexadorHash a("./StopWordsEspanyol.txt",
    ",;:.-/+*\\ '\"{}[]()<>�!�?&#=\t\n\r@", false, false,
    "./indicePrueba", 0, false, true);

    if (a.IndexarDirectorio(nombre_doc)) {
    cout << "Indexacion terminada" << endl;
    cout << "Numero palabras indexadas " << a.NumPalIndexadas() << endl;

    } else
    cout << "Indexacion NO terminada" << endl;
    cout << "Indexar corpus ha tardado " << (getcputime() - aa) << " segundos"
    << endl;

    cout << "Indice Contenido" << endl;
    aa = getcputime();
    IndexadorHash b("./StopWordsEspanyol.txt",
    ",;:.-/+*\\ '\"{}[]()<>�!�?&#=\t\n\r@", false, false,
    "./indicePrueba", 0, false, true);

    if (b.IndexarDirectorio2(nombre_doc)) {
    cout << "Indexacion terminada" << endl;
    cout << "Numero palabras indexadas " << b.NumPalIndexadas() << endl;

    } else
    cout << "Indexacion NO terminada" << endl;
    cout << "Indexar corpus ha tardado " << (getcputime() - aa) << " segundos"
    << endl;


    cout << "Indice Listado" << endl;
    aa = getcputime();
    IndexadorHash c("./StopWordsEspanyol.txt",
    ",;:.-/+*\\ '\"{}[]()<>�!�?&#=\t\n\r@", false, false,
    "./indicePrueba", 0, false, true);

    if (c.IndexarDirectorio3(nombre_doc)) {
    cout << "Indexacion terminada" << endl;
    cout << "Numero palabras indexadas " << c.NumPalIndexadas() << endl;

    } else
    cout << "Indexacion NO terminada" << endl;
    cout << "Indexar corpus ha tardado " << (getcputime() - aa) << " segundos"
    << endl;
    */

   IndexadorHash b("./StopWordsEspanyol.txt", ". ,:", false, true,"./indicePrueba", 0, true, true);

   b.IndexarDirectorio("./materiales_buscador/CorpusTime/Documentos");
   b.GuardarIndexacion();

   InformacionPregunta infPreg1;
   InformacionTermino inf1;



   Buscador a("./indicePrueba", 0);

   
   cout <<"B:"<<endl;
   cout <<b<<endl;

   cout <<"A:"<<endl;
   cout <<a<<endl;


   string preg;
   double kk1;
   double kb;

  // string pregunta = "KENNEDY";
  // int numero_salidas = 4;

  // a.IndexarPregunta(pregunta);

   a.ListarInfColeccDocs();
   /*if (a.Devuelve("kennedy", inf1)) {
     cout << "SE HA INDEXADO " << inf1 << endl;

   }else{
     cout <<"No se ha index"<<endl;
   }*/
 //  return 2;

 cout <<endl;
 cout <<endl;
 cout <<endl;
  
  cout <<"Empiezo a indexar las preguntas para DFR"<<endl;
  a.Buscar("./materiales_buscador/CorpusTime/Preguntas/",423,1,2);
  cout <<"Escribiendo en el fichero..."<<endl;
  a.ImprimirResultadoBusqueda(423,"salidaDFR.txt");


  a.CambiarFormulaSimilitud(1);


  cout <<"Empiezo a indexar las preguntas para BM25"<<endl;
  a.Buscar("./materiales_buscador/CorpusTime/Preguntas/",423,1,2);
  cout <<"Escribiendo el fichero..."<<endl;
  a.ImprimirResultadoBusqueda(423,"salidaBM25.txt");
 
  

 /*
   if (a.Buscar(4)) {

      a.ImprimirResultadoBusqueda(numero_salidas);
   }

   a.CambiarFormulaSimilitud(1);

   if (a.Buscar(4)) {
      a.ImprimirResultadoBusqueda(numero_salidas);
   }*/
}
