
Queryid (Num):	1
Total number of documents over all queries
    Retrieved:      423
    Relevant:         7
    Rel_ret:          7
Interpolated Recall - Precision Averages:
    at 0.00       0.0297 
    at 0.10       0.0297 
    at 0.20       0.0297 
    at 0.30       0.0297 
    at 0.40       0.0297 
    at 0.50       0.0230 
    at 0.60       0.0206 
    at 0.70       0.0206 
    at 0.80       0.0178 
    at 0.90       0.0178 
    at 1.00       0.0178 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0199 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0200
  At  500 docs:   0.0140
  At 1000 docs:   0.0070
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	2
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0206 
    at 0.10       0.0206 
    at 0.20       0.0206 
    at 0.30       0.0206 
    at 0.40       0.0206 
    at 0.50       0.0206 
    at 0.60       0.0206 
    at 0.70       0.0206 
    at 0.80       0.0206 
    at 0.90       0.0206 
    at 1.00       0.0206 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0163 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	3
Total number of documents over all queries
    Retrieved:      423
    Relevant:         4
    Rel_ret:          4
Interpolated Recall - Precision Averages:
    at 0.00       0.0309 
    at 0.10       0.0309 
    at 0.20       0.0309 
    at 0.30       0.0309 
    at 0.40       0.0309 
    at 0.50       0.0309 
    at 0.60       0.0309 
    at 0.70       0.0309 
    at 0.80       0.0105 
    at 0.90       0.0105 
    at 1.00       0.0105 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0242 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0300
  At  200 docs:   0.0150
  At  500 docs:   0.0080
  At 1000 docs:   0.0040
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	4
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0444 
    at 0.10       0.0444 
    at 0.20       0.0444 
    at 0.30       0.0444 
    at 0.40       0.0444 
    at 0.50       0.0186 
    at 0.60       0.0186 
    at 0.70       0.0186 
    at 0.80       0.0186 
    at 0.90       0.0137 
    at 1.00       0.0137 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0240 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0150
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	5
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0526 
    at 0.10       0.0526 
    at 0.20       0.0526 
    at 0.30       0.0526 
    at 0.40       0.0526 
    at 0.50       0.0186 
    at 0.60       0.0186 
    at 0.70       0.0186 
    at 0.80       0.0186 
    at 0.90       0.0165 
    at 1.00       0.0165 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0281 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0333
  At  100 docs:   0.0200
  At  200 docs:   0.0150
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	6
Total number of documents over all queries
    Retrieved:      423
    Relevant:         9
    Rel_ret:          9
Interpolated Recall - Precision Averages:
    at 0.00       0.0396 
    at 0.10       0.0396 
    at 0.20       0.0396 
    at 0.30       0.0396 
    at 0.40       0.0396 
    at 0.50       0.0287 
    at 0.60       0.0247 
    at 0.70       0.0236 
    at 0.80       0.0229 
    at 0.90       0.0229 
    at 1.00       0.0229 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0248 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0300
  At  200 docs:   0.0250
  At  500 docs:   0.0180
  At 1000 docs:   0.0090
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	7
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0286 
    at 0.10       0.0286 
    at 0.20       0.0286 
    at 0.30       0.0286 
    at 0.40       0.0286 
    at 0.50       0.0286 
    at 0.60       0.0278 
    at 0.70       0.0278 
    at 0.80       0.0278 
    at 0.90       0.0278 
    at 1.00       0.0278 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0282 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	8
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0064 
    at 0.10       0.0064 
    at 0.20       0.0064 
    at 0.30       0.0064 
    at 0.40       0.0064 
    at 0.50       0.0064 
    at 0.60       0.0064 
    at 0.70       0.0064 
    at 0.80       0.0064 
    at 0.90       0.0064 
    at 1.00       0.0064 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0050 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	9
Total number of documents over all queries
    Retrieved:      423
    Relevant:         8
    Rel_ret:          8
Interpolated Recall - Precision Averages:
    at 0.00       0.0225 
    at 0.10       0.0225 
    at 0.20       0.0225 
    at 0.30       0.0225 
    at 0.40       0.0225 
    at 0.50       0.0225 
    at 0.60       0.0225 
    at 0.70       0.0225 
    at 0.80       0.0225 
    at 0.90       0.0197 
    at 1.00       0.0197 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0190 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0150
  At  500 docs:   0.0160
  At 1000 docs:   0.0080
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	10
Total number of documents over all queries
    Retrieved:      423
    Relevant:         6
    Rel_ret:          6
Interpolated Recall - Precision Averages:
    at 0.00       0.0161 
    at 0.10       0.0161 
    at 0.20       0.0161 
    at 0.30       0.0161 
    at 0.40       0.0161 
    at 0.50       0.0161 
    at 0.60       0.0161 
    at 0.70       0.0161 
    at 0.80       0.0161 
    at 0.90       0.0147 
    at 1.00       0.0147 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0115 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0120
  At 1000 docs:   0.0060
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	11
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0064 
    at 0.10       0.0064 
    at 0.20       0.0064 
    at 0.30       0.0064 
    at 0.40       0.0064 
    at 0.50       0.0064 
    at 0.60       0.0064 
    at 0.70       0.0064 
    at 0.80       0.0064 
    at 0.90       0.0064 
    at 1.00       0.0064 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0054 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	12
Total number of documents over all queries
    Retrieved:      423
    Relevant:         7
    Rel_ret:          7
Interpolated Recall - Precision Averages:
    at 0.00       0.0213 
    at 0.10       0.0213 
    at 0.20       0.0193 
    at 0.30       0.0193 
    at 0.40       0.0193 
    at 0.50       0.0193 
    at 0.60       0.0193 
    at 0.70       0.0193 
    at 0.80       0.0193 
    at 0.90       0.0172 
    at 1.00       0.0172 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0160 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0140
  At 1000 docs:   0.0070
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	13
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0104 
    at 0.10       0.0104 
    at 0.20       0.0104 
    at 0.30       0.0104 
    at 0.40       0.0104 
    at 0.50       0.0104 
    at 0.60       0.0104 
    at 0.70       0.0104 
    at 0.80       0.0104 
    at 0.90       0.0104 
    at 1.00       0.0104 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0077 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	14
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0040 
    at 0.10       0.0040 
    at 0.20       0.0040 
    at 0.30       0.0040 
    at 0.40       0.0040 
    at 0.50       0.0040 
    at 0.60       0.0040 
    at 0.70       0.0040 
    at 0.80       0.0040 
    at 0.90       0.0040 
    at 1.00       0.0040 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0040 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	15
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          4
Interpolated Recall - Precision Averages:
    at 0.00       0.0114 
    at 0.10       0.0114 
    at 0.20       0.0114 
    at 0.30       0.0114 
    at 0.40       0.0114 
    at 0.50       0.0114 
    at 0.60       0.0114 
    at 0.70       0.0114 
    at 0.80       0.0114 
    at 0.90       0.0000 
    at 1.00       0.0000 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0061 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0080
  At 1000 docs:   0.0040
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	16
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0714 
    at 0.10       0.0714 
    at 0.20       0.0714 
    at 0.30       0.0714 
    at 0.40       0.0110 
    at 0.50       0.0110 
    at 0.60       0.0110 
    at 0.70       0.0079 
    at 0.80       0.0079 
    at 0.90       0.0079 
    at 1.00       0.0079 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0301 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0333
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	17
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0076 
    at 0.10       0.0076 
    at 0.20       0.0076 
    at 0.30       0.0076 
    at 0.40       0.0076 
    at 0.50       0.0076 
    at 0.60       0.0070 
    at 0.70       0.0070 
    at 0.80       0.0070 
    at 0.90       0.0070 
    at 1.00       0.0070 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0073 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	18
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0175 
    at 0.10       0.0175 
    at 0.20       0.0175 
    at 0.30       0.0175 
    at 0.40       0.0175 
    at 0.50       0.0175 
    at 0.60       0.0175 
    at 0.70       0.0175 
    at 0.80       0.0175 
    at 0.90       0.0175 
    at 1.00       0.0175 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0175 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	19
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0194 
    at 0.10       0.0194 
    at 0.20       0.0194 
    at 0.30       0.0194 
    at 0.40       0.0194 
    at 0.50       0.0128 
    at 0.60       0.0128 
    at 0.70       0.0128 
    at 0.80       0.0128 
    at 0.90       0.0128 
    at 1.00       0.0128 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0139 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	20
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0175 
    at 0.10       0.0175 
    at 0.20       0.0175 
    at 0.30       0.0175 
    at 0.40       0.0175 
    at 0.50       0.0175 
    at 0.60       0.0175 
    at 0.70       0.0175 
    at 0.80       0.0175 
    at 0.90       0.0175 
    at 1.00       0.0175 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0175 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	21
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0076 
    at 0.10       0.0076 
    at 0.20       0.0076 
    at 0.30       0.0076 
    at 0.40       0.0076 
    at 0.50       0.0076 
    at 0.60       0.0056 
    at 0.70       0.0056 
    at 0.80       0.0056 
    at 0.90       0.0056 
    at 1.00       0.0056 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0066 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	22
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0282 
    at 0.10       0.0282 
    at 0.20       0.0282 
    at 0.30       0.0282 
    at 0.40       0.0282 
    at 0.50       0.0282 
    at 0.60       0.0282 
    at 0.70       0.0282 
    at 0.80       0.0282 
    at 0.90       0.0282 
    at 1.00       0.0282 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0229 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	23
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          0
Interpolated Recall - Precision Averages:
    at 0.00       0.0000 
    at 0.10       0.0000 
    at 0.20       0.0000 
    at 0.30       0.0000 
    at 0.40       0.0000 
    at 0.50       0.0000 
    at 0.60       0.0000 
    at 0.70       0.0000 
    at 0.80       0.0000 
    at 0.90       0.0000 
    at 1.00       0.0000 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0000 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0000
  At 1000 docs:   0.0000
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	24
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0028 
    at 0.10       0.0028 
    at 0.20       0.0028 
    at 0.30       0.0028 
    at 0.40       0.0028 
    at 0.50       0.0028 
    at 0.60       0.0028 
    at 0.70       0.0028 
    at 0.80       0.0028 
    at 0.90       0.0028 
    at 1.00       0.0028 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0028 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	25
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0042 
    at 0.10       0.0042 
    at 0.20       0.0042 
    at 0.30       0.0042 
    at 0.40       0.0042 
    at 0.50       0.0042 
    at 0.60       0.0042 
    at 0.70       0.0042 
    at 0.80       0.0042 
    at 0.90       0.0042 
    at 1.00       0.0042 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0042 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	26
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0028 
    at 0.10       0.0028 
    at 0.20       0.0028 
    at 0.30       0.0028 
    at 0.40       0.0028 
    at 0.50       0.0028 
    at 0.60       0.0000 
    at 0.70       0.0000 
    at 0.80       0.0000 
    at 0.90       0.0000 
    at 1.00       0.0000 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0014 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	27
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0185 
    at 0.10       0.0185 
    at 0.20       0.0185 
    at 0.30       0.0185 
    at 0.40       0.0185 
    at 0.50       0.0185 
    at 0.60       0.0185 
    at 0.70       0.0185 
    at 0.80       0.0185 
    at 0.90       0.0185 
    at 1.00       0.0185 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0135 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0150
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	28
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0208 
    at 0.10       0.0208 
    at 0.20       0.0208 
    at 0.30       0.0208 
    at 0.40       0.0208 
    at 0.50       0.0208 
    at 0.60       0.0208 
    at 0.70       0.0208 
    at 0.80       0.0208 
    at 0.90       0.0147 
    at 1.00       0.0147 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0164 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0200
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	29
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0164 
    at 0.10       0.0164 
    at 0.20       0.0164 
    at 0.30       0.0164 
    at 0.40       0.0164 
    at 0.50       0.0164 
    at 0.60       0.0098 
    at 0.70       0.0098 
    at 0.80       0.0098 
    at 0.90       0.0098 
    at 1.00       0.0098 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0131 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	30
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0210 
    at 0.10       0.0210 
    at 0.20       0.0210 
    at 0.30       0.0210 
    at 0.40       0.0210 
    at 0.50       0.0210 
    at 0.60       0.0210 
    at 0.70       0.0152 
    at 0.80       0.0152 
    at 0.90       0.0152 
    at 1.00       0.0152 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0179 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0150
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	31
Total number of documents over all queries
    Retrieved:      423
    Relevant:         7
    Rel_ret:          7
Interpolated Recall - Precision Averages:
    at 0.00       0.1667 
    at 0.10       0.1667 
    at 0.20       0.1667 
    at 0.30       0.0455 
    at 0.40       0.0455 
    at 0.50       0.0370 
    at 0.60       0.0312 
    at 0.70       0.0312 
    at 0.80       0.0238 
    at 0.90       0.0201 
    at 1.00       0.0201 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0622 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.1000
  At   15 docs:   0.1333
  At   20 docs:   0.1000
  At   30 docs:   0.0667
  At  100 docs:   0.0300
  At  200 docs:   0.0250
  At  500 docs:   0.0140
  At 1000 docs:   0.0070
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	32
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0030 
    at 0.10       0.0030 
    at 0.20       0.0030 
    at 0.30       0.0030 
    at 0.40       0.0030 
    at 0.50       0.0030 
    at 0.60       0.0030 
    at 0.70       0.0030 
    at 0.80       0.0030 
    at 0.90       0.0030 
    at 1.00       0.0030 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0030 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	33
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0120 
    at 0.10       0.0120 
    at 0.20       0.0120 
    at 0.30       0.0120 
    at 0.40       0.0120 
    at 0.50       0.0120 
    at 0.60       0.0109 
    at 0.70       0.0109 
    at 0.80       0.0109 
    at 0.90       0.0109 
    at 1.00       0.0109 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0115 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	34
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0043 
    at 0.10       0.0043 
    at 0.20       0.0043 
    at 0.30       0.0043 
    at 0.40       0.0043 
    at 0.50       0.0043 
    at 0.60       0.0043 
    at 0.70       0.0043 
    at 0.80       0.0043 
    at 0.90       0.0043 
    at 1.00       0.0043 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0043 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	35
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0028 
    at 0.10       0.0028 
    at 0.20       0.0028 
    at 0.30       0.0028 
    at 0.40       0.0028 
    at 0.50       0.0028 
    at 0.60       0.0028 
    at 0.70       0.0028 
    at 0.80       0.0028 
    at 0.90       0.0028 
    at 1.00       0.0028 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0028 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	36
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0058 
    at 0.10       0.0058 
    at 0.20       0.0058 
    at 0.30       0.0058 
    at 0.40       0.0058 
    at 0.50       0.0058 
    at 0.60       0.0058 
    at 0.70       0.0058 
    at 0.80       0.0058 
    at 0.90       0.0058 
    at 1.00       0.0058 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0058 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	37
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0190 
    at 0.10       0.0190 
    at 0.20       0.0190 
    at 0.30       0.0190 
    at 0.40       0.0190 
    at 0.50       0.0190 
    at 0.60       0.0190 
    at 0.70       0.0190 
    at 0.80       0.0190 
    at 0.90       0.0190 
    at 1.00       0.0190 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0154 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	38
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0026 
    at 0.10       0.0026 
    at 0.20       0.0026 
    at 0.30       0.0026 
    at 0.40       0.0026 
    at 0.50       0.0026 
    at 0.60       0.0026 
    at 0.70       0.0026 
    at 0.80       0.0026 
    at 0.90       0.0026 
    at 1.00       0.0026 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0026 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	39
Total number of documents over all queries
    Retrieved:      423
    Relevant:         9
    Rel_ret:          9
Interpolated Recall - Precision Averages:
    at 0.00       0.2000 
    at 0.10       0.2000 
    at 0.20       0.0667 
    at 0.30       0.0556 
    at 0.40       0.0365 
    at 0.50       0.0365 
    at 0.60       0.0365 
    at 0.70       0.0365 
    at 0.80       0.0365 
    at 0.90       0.0265 
    at 1.00       0.0265 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0558 
Precision:
  At    5 docs:   0.2000
  At   10 docs:   0.1000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0667
  At  100 docs:   0.0300
  At  200 docs:   0.0350
  At  500 docs:   0.0180
  At 1000 docs:   0.0090
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.1111

Queryid (Num):	40
Total number of documents over all queries
    Retrieved:      423
    Relevant:         9
    Rel_ret:          9
Interpolated Recall - Precision Averages:
    at 0.00       0.0283 
    at 0.10       0.0283 
    at 0.20       0.0283 
    at 0.30       0.0283 
    at 0.40       0.0283 
    at 0.50       0.0283 
    at 0.60       0.0283 
    at 0.70       0.0283 
    at 0.80       0.0283 
    at 0.90       0.0238 
    at 1.00       0.0238 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0194 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0150
  At  500 docs:   0.0180
  At 1000 docs:   0.0090
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	41
Total number of documents over all queries
    Retrieved:      423
    Relevant:         6
    Rel_ret:          6
Interpolated Recall - Precision Averages:
    at 0.00       0.0909 
    at 0.10       0.0909 
    at 0.20       0.0462 
    at 0.30       0.0462 
    at 0.40       0.0462 
    at 0.50       0.0462 
    at 0.60       0.0354 
    at 0.70       0.0189 
    at 0.80       0.0189 
    at 0.90       0.0152 
    at 1.00       0.0152 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0414 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0333
  At  100 docs:   0.0300
  At  200 docs:   0.0200
  At  500 docs:   0.0120
  At 1000 docs:   0.0060
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	42
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0062 
    at 0.10       0.0062 
    at 0.20       0.0062 
    at 0.30       0.0062 
    at 0.40       0.0062 
    at 0.50       0.0062 
    at 0.60       0.0062 
    at 0.70       0.0062 
    at 0.80       0.0062 
    at 0.90       0.0062 
    at 1.00       0.0062 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0062 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	43
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0164 
    at 0.10       0.0164 
    at 0.20       0.0164 
    at 0.30       0.0164 
    at 0.40       0.0164 
    at 0.50       0.0164 
    at 0.60       0.0149 
    at 0.70       0.0149 
    at 0.80       0.0149 
    at 0.90       0.0149 
    at 1.00       0.0149 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0157 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	44
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0143 
    at 0.10       0.0143 
    at 0.20       0.0143 
    at 0.30       0.0143 
    at 0.40       0.0143 
    at 0.50       0.0143 
    at 0.60       0.0143 
    at 0.70       0.0143 
    at 0.80       0.0143 
    at 0.90       0.0143 
    at 1.00       0.0143 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0116 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	45
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0278 
    at 0.10       0.0278 
    at 0.20       0.0278 
    at 0.30       0.0145 
    at 0.40       0.0145 
    at 0.50       0.0145 
    at 0.60       0.0145 
    at 0.70       0.0145 
    at 0.80       0.0145 
    at 0.90       0.0145 
    at 1.00       0.0145 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0155 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	46
Total number of documents over all queries
    Retrieved:      423
    Relevant:        18
    Rel_ret:         18
Interpolated Recall - Precision Averages:
    at 0.00       0.1200 
    at 0.10       0.1200 
    at 0.20       0.0896 
    at 0.30       0.0896 
    at 0.40       0.0741 
    at 0.50       0.0714 
    at 0.60       0.0670 
    at 0.70       0.0670 
    at 0.80       0.0628 
    at 0.90       0.0582 
    at 1.00       0.0456 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0730 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.1000
  At   30 docs:   0.1000
  At  100 docs:   0.0700
  At  200 docs:   0.0650
  At  500 docs:   0.0360
  At 1000 docs:   0.0180
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0556

Queryid (Num):	47
Total number of documents over all queries
    Retrieved:      423
    Relevant:         6
    Rel_ret:          6
Interpolated Recall - Precision Averages:
    at 0.00       0.1667 
    at 0.10       0.1667 
    at 0.20       0.1667 
    at 0.30       0.1667 
    at 0.40       0.0615 
    at 0.50       0.0615 
    at 0.60       0.0615 
    at 0.70       0.0311 
    at 0.80       0.0311 
    at 0.90       0.0227 
    at 1.00       0.0227 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0753 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.1000
  At   15 docs:   0.1333
  At   20 docs:   0.1000
  At   30 docs:   0.0667
  At  100 docs:   0.0400
  At  200 docs:   0.0250
  At  500 docs:   0.0120
  At 1000 docs:   0.0060
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	48
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0078 
    at 0.10       0.0078 
    at 0.20       0.0078 
    at 0.30       0.0078 
    at 0.40       0.0078 
    at 0.50       0.0078 
    at 0.60       0.0078 
    at 0.70       0.0078 
    at 0.80       0.0078 
    at 0.90       0.0078 
    at 1.00       0.0078 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0078 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	49
Total number of documents over all queries
    Retrieved:      423
    Relevant:         8
    Rel_ret:          8
Interpolated Recall - Precision Averages:
    at 0.00       0.1667 
    at 0.10       0.1667 
    at 0.20       0.1667 
    at 0.30       0.0455 
    at 0.40       0.0370 
    at 0.50       0.0370 
    at 0.60       0.0359 
    at 0.70       0.0359 
    at 0.80       0.0278 
    at 0.90       0.0229 
    at 1.00       0.0229 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0598 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.1000
  At   15 docs:   0.1333
  At   20 docs:   0.1000
  At   30 docs:   0.0667
  At  100 docs:   0.0300
  At  200 docs:   0.0300
  At  500 docs:   0.0160
  At 1000 docs:   0.0080
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	50
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0056 
    at 0.10       0.0056 
    at 0.20       0.0056 
    at 0.30       0.0056 
    at 0.40       0.0056 
    at 0.50       0.0056 
    at 0.60       0.0056 
    at 0.70       0.0056 
    at 0.80       0.0056 
    at 0.90       0.0056 
    at 1.00       0.0056 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0056 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	51
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0094 
    at 0.10       0.0094 
    at 0.20       0.0094 
    at 0.30       0.0094 
    at 0.40       0.0094 
    at 0.50       0.0094 
    at 0.60       0.0094 
    at 0.70       0.0094 
    at 0.80       0.0094 
    at 0.90       0.0094 
    at 1.00       0.0094 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0073 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	52
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0067 
    at 0.10       0.0067 
    at 0.20       0.0067 
    at 0.30       0.0067 
    at 0.40       0.0067 
    at 0.50       0.0067 
    at 0.60       0.0067 
    at 0.70       0.0067 
    at 0.80       0.0067 
    at 0.90       0.0067 
    at 1.00       0.0067 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0059 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	53
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0143 
    at 0.10       0.0143 
    at 0.20       0.0143 
    at 0.30       0.0143 
    at 0.40       0.0143 
    at 0.50       0.0143 
    at 0.60       0.0143 
    at 0.70       0.0143 
    at 0.80       0.0143 
    at 0.90       0.0143 
    at 1.00       0.0143 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0110 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	54
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0177 
    at 0.10       0.0177 
    at 0.20       0.0177 
    at 0.30       0.0177 
    at 0.40       0.0177 
    at 0.50       0.0177 
    at 0.60       0.0177 
    at 0.70       0.0177 
    at 0.80       0.0177 
    at 0.90       0.0177 
    at 1.00       0.0177 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0147 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	55
Total number of documents over all queries
    Retrieved:      423
    Relevant:        12
    Rel_ret:         12
Interpolated Recall - Precision Averages:
    at 0.00       0.1176 
    at 0.10       0.1176 
    at 0.20       0.0741 
    at 0.30       0.0741 
    at 0.40       0.0490 
    at 0.50       0.0472 
    at 0.60       0.0472 
    at 0.70       0.0472 
    at 0.80       0.0472 
    at 0.90       0.0345 
    at 1.00       0.0345 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0555 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.1000
  At   30 docs:   0.0667
  At  100 docs:   0.0400
  At  200 docs:   0.0450
  At  500 docs:   0.0240
  At 1000 docs:   0.0120
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0833

Queryid (Num):	56
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0081 
    at 0.10       0.0081 
    at 0.20       0.0081 
    at 0.30       0.0081 
    at 0.40       0.0081 
    at 0.50       0.0081 
    at 0.60       0.0081 
    at 0.70       0.0081 
    at 0.80       0.0081 
    at 0.90       0.0081 
    at 1.00       0.0081 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0081 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	57
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0155 
    at 0.10       0.0155 
    at 0.20       0.0155 
    at 0.30       0.0155 
    at 0.40       0.0155 
    at 0.50       0.0155 
    at 0.60       0.0155 
    at 0.70       0.0155 
    at 0.80       0.0155 
    at 0.90       0.0155 
    at 1.00       0.0155 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0120 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	58
Total number of documents over all queries
    Retrieved:      423
    Relevant:         8
    Rel_ret:          8
Interpolated Recall - Precision Averages:
    at 0.00       0.0833 
    at 0.10       0.0833 
    at 0.20       0.0392 
    at 0.30       0.0294 
    at 0.40       0.0283 
    at 0.50       0.0283 
    at 0.60       0.0283 
    at 0.70       0.0283 
    at 0.80       0.0230 
    at 0.90       0.0230 
    at 1.00       0.0230 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0340 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0333
  At  100 docs:   0.0200
  At  200 docs:   0.0250
  At  500 docs:   0.0160
  At 1000 docs:   0.0080
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	59
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0294 
    at 0.10       0.0294 
    at 0.20       0.0294 
    at 0.30       0.0294 
    at 0.40       0.0294 
    at 0.50       0.0294 
    at 0.60       0.0171 
    at 0.70       0.0171 
    at 0.80       0.0171 
    at 0.90       0.0171 
    at 1.00       0.0171 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0233 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	60
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0171 
    at 0.10       0.0171 
    at 0.20       0.0171 
    at 0.30       0.0171 
    at 0.40       0.0171 
    at 0.50       0.0171 
    at 0.60       0.0171 
    at 0.70       0.0171 
    at 0.80       0.0171 
    at 0.90       0.0171 
    at 1.00       0.0171 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0130 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	61
Total number of documents over all queries
    Retrieved:      423
    Relevant:        15
    Rel_ret:         15
Interpolated Recall - Precision Averages:
    at 0.00       0.0833 
    at 0.10       0.0612 
    at 0.20       0.0612 
    at 0.30       0.0529 
    at 0.40       0.0529 
    at 0.50       0.0529 
    at 0.60       0.0529 
    at 0.70       0.0529 
    at 0.80       0.0529 
    at 0.90       0.0403 
    at 1.00       0.0380 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0516 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0333
  At  100 docs:   0.0400
  At  200 docs:   0.0500
  At  500 docs:   0.0300
  At 1000 docs:   0.0150
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0667

Queryid (Num):	62
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.2500 
    at 0.10       0.2500 
    at 0.20       0.2500 
    at 0.30       0.2500 
    at 0.40       0.2500 
    at 0.50       0.2500 
    at 0.60       0.2500 
    at 0.70       0.2500 
    at 0.80       0.2500 
    at 0.90       0.2500 
    at 1.00       0.2500 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.2500 
Precision:
  At    5 docs:   0.2000
  At   10 docs:   0.1000
  At   15 docs:   0.0667
  At   20 docs:   0.0500
  At   30 docs:   0.0333
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	63
Total number of documents over all queries
    Retrieved:      423
    Relevant:        11
    Rel_ret:         11
Interpolated Recall - Precision Averages:
    at 0.00       0.0488 
    at 0.10       0.0488 
    at 0.20       0.0488 
    at 0.30       0.0488 
    at 0.40       0.0394 
    at 0.50       0.0315 
    at 0.60       0.0315 
    at 0.70       0.0281 
    at 0.80       0.0281 
    at 0.90       0.0281 
    at 1.00       0.0281 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0318 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0400
  At  200 docs:   0.0250
  At  500 docs:   0.0220
  At 1000 docs:   0.0110
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	64
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0192 
    at 0.10       0.0192 
    at 0.20       0.0192 
    at 0.30       0.0192 
    at 0.40       0.0192 
    at 0.50       0.0192 
    at 0.60       0.0192 
    at 0.70       0.0192 
    at 0.80       0.0192 
    at 0.90       0.0192 
    at 1.00       0.0192 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0149 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	65
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0042 
    at 0.10       0.0042 
    at 0.20       0.0042 
    at 0.30       0.0042 
    at 0.40       0.0042 
    at 0.50       0.0042 
    at 0.60       0.0042 
    at 0.70       0.0042 
    at 0.80       0.0042 
    at 0.90       0.0042 
    at 1.00       0.0042 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0042 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	66
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0063 
    at 0.10       0.0063 
    at 0.20       0.0063 
    at 0.30       0.0063 
    at 0.40       0.0063 
    at 0.50       0.0063 
    at 0.60       0.0051 
    at 0.70       0.0051 
    at 0.80       0.0051 
    at 0.90       0.0051 
    at 1.00       0.0051 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0057 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	67
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0072 
    at 0.10       0.0072 
    at 0.20       0.0072 
    at 0.30       0.0072 
    at 0.40       0.0072 
    at 0.50       0.0072 
    at 0.60       0.0072 
    at 0.70       0.0072 
    at 0.80       0.0072 
    at 0.90       0.0072 
    at 1.00       0.0072 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0061 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0050
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	68
Total number of documents over all queries
    Retrieved:      423
    Relevant:         8
    Rel_ret:          8
Interpolated Recall - Precision Averages:
    at 0.00       0.0280 
    at 0.10       0.0280 
    at 0.20       0.0280 
    at 0.30       0.0280 
    at 0.40       0.0191 
    at 0.50       0.0191 
    at 0.60       0.0191 
    at 0.70       0.0191 
    at 0.80       0.0191 
    at 0.90       0.0191 
    at 1.00       0.0191 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0181 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0150
  At  500 docs:   0.0160
  At 1000 docs:   0.0080
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	69
Total number of documents over all queries
    Retrieved:      423
    Relevant:        13
    Rel_ret:         13
Interpolated Recall - Precision Averages:
    at 0.00       0.0315 
    at 0.10       0.0315 
    at 0.20       0.0315 
    at 0.30       0.0315 
    at 0.40       0.0315 
    at 0.50       0.0315 
    at 0.60       0.0315 
    at 0.70       0.0315 
    at 0.80       0.0315 
    at 0.90       0.0315 
    at 1.00       0.0315 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0255 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0200
  At  200 docs:   0.0200
  At  500 docs:   0.0260
  At 1000 docs:   0.0130
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	70
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0028 
    at 0.10       0.0028 
    at 0.20       0.0028 
    at 0.30       0.0028 
    at 0.40       0.0028 
    at 0.50       0.0028 
    at 0.60       0.0028 
    at 0.70       0.0028 
    at 0.80       0.0028 
    at 0.90       0.0028 
    at 1.00       0.0028 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0028 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	71
Total number of documents over all queries
    Retrieved:      423
    Relevant:         3
    Rel_ret:          3
Interpolated Recall - Precision Averages:
    at 0.00       0.0417 
    at 0.10       0.0417 
    at 0.20       0.0417 
    at 0.30       0.0417 
    at 0.40       0.0086 
    at 0.50       0.0086 
    at 0.60       0.0086 
    at 0.70       0.0086 
    at 0.80       0.0086 
    at 0.90       0.0086 
    at 1.00       0.0086 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0187 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0333
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0060
  At 1000 docs:   0.0030
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	72
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0041 
    at 0.10       0.0041 
    at 0.20       0.0041 
    at 0.30       0.0041 
    at 0.40       0.0041 
    at 0.50       0.0041 
    at 0.60       0.0041 
    at 0.70       0.0041 
    at 0.80       0.0041 
    at 0.90       0.0041 
    at 1.00       0.0041 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0041 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	73
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0032 
    at 0.10       0.0032 
    at 0.20       0.0032 
    at 0.30       0.0032 
    at 0.40       0.0032 
    at 0.50       0.0032 
    at 0.60       0.0032 
    at 0.70       0.0032 
    at 0.80       0.0032 
    at 0.90       0.0032 
    at 1.00       0.0032 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0032 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	74
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0067 
    at 0.10       0.0067 
    at 0.20       0.0067 
    at 0.30       0.0067 
    at 0.40       0.0067 
    at 0.50       0.0067 
    at 0.60       0.0067 
    at 0.70       0.0067 
    at 0.80       0.0067 
    at 0.90       0.0067 
    at 1.00       0.0067 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0057 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	75
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0312 
    at 0.10       0.0312 
    at 0.20       0.0312 
    at 0.30       0.0312 
    at 0.40       0.0312 
    at 0.50       0.0312 
    at 0.60       0.0312 
    at 0.70       0.0312 
    at 0.80       0.0312 
    at 0.90       0.0312 
    at 1.00       0.0312 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0312 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0050
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	76
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0167 
    at 0.10       0.0167 
    at 0.20       0.0167 
    at 0.30       0.0167 
    at 0.40       0.0167 
    at 0.50       0.0167 
    at 0.60       0.0167 
    at 0.70       0.0167 
    at 0.80       0.0167 
    at 0.90       0.0167 
    at 1.00       0.0167 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0116 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	77
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0028 
    at 0.10       0.0028 
    at 0.20       0.0028 
    at 0.30       0.0028 
    at 0.40       0.0028 
    at 0.50       0.0028 
    at 0.60       0.0028 
    at 0.70       0.0028 
    at 0.80       0.0028 
    at 0.90       0.0028 
    at 1.00       0.0028 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0028 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	78
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0058 
    at 0.10       0.0058 
    at 0.20       0.0058 
    at 0.30       0.0058 
    at 0.40       0.0058 
    at 0.50       0.0058 
    at 0.60       0.0058 
    at 0.70       0.0058 
    at 0.80       0.0058 
    at 0.90       0.0058 
    at 1.00       0.0058 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0049 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	79
Total number of documents over all queries
    Retrieved:      423
    Relevant:         1
    Rel_ret:          1
Interpolated Recall - Precision Averages:
    at 0.00       0.0032 
    at 0.10       0.0032 
    at 0.20       0.0032 
    at 0.30       0.0032 
    at 0.40       0.0032 
    at 0.50       0.0032 
    at 0.60       0.0032 
    at 0.70       0.0032 
    at 0.80       0.0032 
    at 0.90       0.0032 
    at 1.00       0.0032 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0032 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0020
  At 1000 docs:   0.0010
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	80
Total number of documents over all queries
    Retrieved:      423
    Relevant:        17
    Rel_ret:         17
Interpolated Recall - Precision Averages:
    at 0.00       0.0833 
    at 0.10       0.0833 
    at 0.20       0.0672 
    at 0.30       0.0672 
    at 0.40       0.0672 
    at 0.50       0.0672 
    at 0.60       0.0659 
    at 0.70       0.0659 
    at 0.80       0.0476 
    at 0.90       0.0476 
    at 1.00       0.0439 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0535 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0667
  At  100 docs:   0.0300
  At  200 docs:   0.0600
  At  500 docs:   0.0340
  At 1000 docs:   0.0170
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	81
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0064 
    at 0.10       0.0064 
    at 0.20       0.0064 
    at 0.30       0.0064 
    at 0.40       0.0064 
    at 0.50       0.0064 
    at 0.60       0.0064 
    at 0.70       0.0064 
    at 0.80       0.0064 
    at 0.90       0.0064 
    at 1.00       0.0064 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0054 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0000
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	82
Total number of documents over all queries
    Retrieved:      423
    Relevant:         5
    Rel_ret:          5
Interpolated Recall - Precision Averages:
    at 0.00       0.0234 
    at 0.10       0.0234 
    at 0.20       0.0234 
    at 0.30       0.0234 
    at 0.40       0.0234 
    at 0.50       0.0234 
    at 0.60       0.0234 
    at 0.70       0.0234 
    at 0.80       0.0234 
    at 0.90       0.0152 
    at 1.00       0.0152 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0173 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0000
  At  200 docs:   0.0200
  At  500 docs:   0.0100
  At 1000 docs:   0.0050
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	83
Total number of documents over all queries
    Retrieved:      423
    Relevant:         2
    Rel_ret:          2
Interpolated Recall - Precision Averages:
    at 0.00       0.0204 
    at 0.10       0.0204 
    at 0.20       0.0204 
    at 0.30       0.0204 
    at 0.40       0.0204 
    at 0.50       0.0204 
    at 0.60       0.0150 
    at 0.70       0.0150 
    at 0.80       0.0150 
    at 0.90       0.0150 
    at 1.00       0.0150 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0177 
Precision:
  At    5 docs:   0.0000
  At   10 docs:   0.0000
  At   15 docs:   0.0000
  At   20 docs:   0.0000
  At   30 docs:   0.0000
  At  100 docs:   0.0100
  At  200 docs:   0.0100
  At  500 docs:   0.0040
  At 1000 docs:   0.0020
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0000

Queryid (Num):	All
Total number of documents over all queries
    Retrieved:    35109
    Relevant:       324
    Rel_ret:        321
Interpolated Recall - Precision Averages:
    at 0.00       0.0327 
    at 0.10       0.0325 
    at 0.20       0.0287 
    at 0.30       0.0252 
    at 0.40       0.0218 
    at 0.50       0.0205 
    at 0.60       0.0198 
    at 0.70       0.0190 
    at 0.80       0.0182 
    at 0.90       0.0170 
    at 1.00       0.0168 
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.0204 
Precision:
  At    5 docs:   0.0048
  At   10 docs:   0.0060
  At   15 docs:   0.0112
  At   20 docs:   0.0096
  At   30 docs:   0.0088
  At  100 docs:   0.0100
  At  200 docs:   0.0109
  At  500 docs:   0.0077
  At 1000 docs:   0.0039
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.0038
