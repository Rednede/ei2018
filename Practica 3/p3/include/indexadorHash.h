/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   indexadorHash.h
 * Author: lourdes
 *
 * Created on April 19, 2018, 11:20 AM
 */
#ifndef INDEXADORHASH_H
#define INDEXADORHASH_H
#include <fstream>
#include <iostream>
#include "./indexadorInformacion.h"
#include "./tokenizador.h"
#include <unordered_set>
#include <unordered_map>

class IndexadorHash{
    friend std::ostream& operator<<(std::ostream& s, const IndexadorHash& p){
        s << "Fichero con el listado de palabras de parada: " << p.ficheroStopWords << std::endl;
        s << "Tokenizador: " << p.tok << std::endl;
        s << "Directorio donde se almacenara el indice generado: " << p.directorioIndice << std::endl;
        s << "Stemmer utilizado: " << p.tipoStemmer << std::endl;
        s << "Informacion de la coleccion indexada: " << p.informacionColeccionDocs << std::endl;
        s << "Se almacenara parte del indice en disco duro: " << p.almacenarEnDisco << std::endl;
        s << "Se almacenaran las posiciones de los terminos: " << p.almacenarPosTerm << std::endl;
        
        return s;
    }
public:
    /*"fichStopwords" ser� el nombre del archivo que contendra todas las palabras de parada (una palabra 
     * por cada linea del fichero) y se almacenar� en el campo privado "ficheroStopWords". Asimismo,
     * almacenar� todas las palabras de parada que contenga el archivo en el campo privado "stopWords", 
     * el indice de palabras con parada.
     * "delimitadores" ser� el string que contiene todos los delimitadores utilizados por el tokenizador
     * (campo privado tok).
     * detectComp y minuscSinAcentos seran los parametros que se pasaran al tokenizador.
     * "dirIndice" sera el directorio del disco duro donde se almacenara el indice (campo privado "directorioIndice").
     * Si dirIndice="" entonces se almacenara en el directorio donde se ejecuta el programa.
     * tStemmer inicializara la variable privada "tipoStemmer":
     *      0 = no se aplica stemmer: se indexa el t�rmino tal y como aparece tokenizado
     *      1 = stemmer de porter para espa�ol
     *      2 = stemmer de porter para ingles
     * "almEnDisco" inicializara la variable privada "almacenarEnDisco"
     * "almPosTerm" inicializara la variable privada "almacenarPosTerm"
     * Los indices (p.ej. indice, indiceDocs e informacionColeccionDocs) quedaran vacios
     */
    IndexadorHash(const std::string& fichStopWords, const std::string& delimitadores, const bool& detectComp, 
            const bool& minuscSinAcentos, const std::string& dirIndice, const int& tStemmer, 
            const bool& almEnDisco, const bool& almPosTerm);
    
    /*Constructor para incializar IndexadorHash a partir e una indexacion previamente
     * realizada que habra sido almacenada en "directorioIndexacion" mediante el metodo 
     * "bool GuardarIndexacion()". Con ello toda la parte privada se inicializara convenientemente,
     * igual que si se acabase de indexar la coleccion de documentos. En caso que no exista 
     * el directorio o que no contenga los datos de la indexacion se tratara la excepcion correspondiente */
    IndexadorHash(const std::string& directorioIndexacion);
    
    IndexadorHash(const IndexadorHash&);
    
    ~IndexadorHash();
    
    IndexadorHash& operator= (const IndexadorHash&);
    
    void AplicaStemmer(std::string& termino);
    
    /*Devuelve true si consigue crear el indice para la coleccion de docuemntos
     * detallada en ficheroDocumentos, el cual contendra un monbre de documento
     * por linea. Los a�adira a los ya existentes anteriormente en el indice
     * Devuelve falso sino  finaliza la indexacion p.ej por falta de memoria, mostrando
     * el mensaje de excepcion correspondiente, indicando el documento y termino en el que se ha quedado
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente indexados
     * (ha de coincidir el nombre del documento y el directorio en que se encuentra),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran 
     * (borrar el documento previamente indexado e indexar el nuevo) en caso que la fecha
     * de modificacion del docuemnto sea mas reciente que la almacenada previamente (class "InfDoc"
     * campo "fechaModificacion"). Los caso de reindexacion mantendr�n el mismo idDoc
     */
    bool Indexar(const std::string& );
    
    /* Devuelve true si consigue crear el indice para la coleccion de documentos
     * que se encuentra en el directorio (y subdirectorios que contenga) dirAIndexar 
     * (independientemente de extension de los mismos). Se considerara que todos los
     * documentos del directorio seran ficheros de texto. Los a�adir� a los ya existentes
     * anteriormente en el indice.
     * Devuelve falso si no finaliza la indexacion p.ej por falta de memoria o porque no
     * exista dirAIndexar, mostrando el mensaje de error correspondiente, indicando el documento y 
     * t�rmino en el que se ha quedado.
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente
     * indexados (ha de coincidir el nombre del documento y el directorio en el que se encuentre),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran (borrar el documento
     * previamente indexado e indexar de nuevo) en caso que la fecha de modificacion del documento 
     * sea mas reciente que la almacenada previamente (class "InfDoc" campo "fechaModificacion").
     * Los casos de reindexacion mantendran el mismo idDoc
     */
    bool IndexarDirectorio(const std::string& dirAIndexar);
    
    /* Se guardara en disco duro (directorio contenido en la variable privada "directorioIndice")
     * la indexacion actualmente en memoria (incluidos todos los parametros de la parte privada).
     * La forma de almacenamiento la determinara el alumno. El objetivo es que esta indexacion
     * se pueda recuperar posteriormente mediante el constructor "IndexadorHash(const string&)".
     * Por ejemplo, supongamos que se ejecuta esta secuencia de comandos: 
     * "IndexadorHash a ("./fichStopWords.txt", "[ ,.", "./dirIndexPrueba", 0, false);
     * "a.Indexar("./fichConDocsAIndexar.txt"); a.GuardarIndexacion();"
     * Entonces mediante el comando: "IndexadorHash b("./dirIndexPrueba");" se recuperara la 
     * indexacion realizada en la secuencia anterior, cargandola en b.
     * Devuelve falso si no finaliza la operacion (p.ej por falta de moemoria o el nombre del directorio
     * contenido en directorioIndice no es correcto), mostrando el mensaje de error correspondiente.
     * en caso de que no existiese el directorio directorioIndice, habr�a que crearlo previamente
     * 
     */
    bool GuardarIndexacion() const;
    
    /* Vacia la indexacion que tuviese en ese momento e inicializa IndexadorHash a partir
     * de una indexacion previamente realizada que habra sido almacenada en "directorioIndexacion"
     * mediante el metoo "bool GuardarIndexaxion();". Con ello toda la parte privada se inicializara
     * convenientemente, igual que si se acabase de indexar la coleccion de documentos.
     * En caso que no existiera el directorio o que no contengalos datos de la indexaci�n
     * se tratar� la excepcion correspondiente, y se devolvera false
     */
    bool RecuperarIndexacion(const std::string& directorioIndexacion);
    
    void ImprimirIndexacion()const;//(el codigo es dado)
    
    /*Devuelve true si consigue crear el indice para la pregunta "preg".
     * antes de realizar la indexacion vaciara los campos privados indicePregunta
     * e infPregunta. Generara la misma informacion que en la indexacion de documentos
     * pero dejandola toda accesible en memoria principal (mediante las variables privadas
     * "pregunta, indicePregunta, infPregunta")
     * Devuelve false si no finaliza la operacion (p.ej por falta de memoria o bien 
     * si la pregunta no contiene ningun termino con contenido), comstrando el mensaje
     * de error correspondiente
     */
    bool IndexarPregunta(const std::string& preg);
    
    /*Devuelve true si hay una pregunta indexada (con al menos un termino que no sea
     * palabra de parada, o sea que haya algun termino indexado en indicePregunta), 
     * devolviendola en "preg"
     */
    bool DevuelvePregunta(std::string& preg) const;
    
    /*Devuelve true si word esta indexado en la pregunta, devolviendo su informacion
     * almacenada "inf". En caso que no este devolveria devolveria "inf" vacio*/
    bool DevuelvePregunta(const std::string& word, InformacionTerminoPregunta& inf)const;
    
    /*Devuelve true si hay una pregunta indexada, devolviendo su informacion almacenada
     * (campo privado "infPregunta")en "inf". En caso que no este, devolveria "inf" vacio
     */
    bool DevuelvePregunta(InformacionPregunta& inf)const;
    
    void ImprimirIndexacionPregunta()const;//es dado
    
    void ImprimirPregunta()const;//es dado
    
    /*Devuelve true si wor esta indexado, devolviendo su informacion almacenada "inf".
     * En caso que no este, devolveria "inf" vacio
     */
    bool Devuelve(const std::string& word, InformacionTermino& inf)const;
    
    /*Devuelve true si word esta indexado y aparece en el documento de nobre nomDoc, 
     * en cuyo caso devuelve la inormacion alamacenada para word en el documento.
     * En caso que noeste, devolveria "InfDoc" vacio
     */
    bool Devuelve(const std::string& word, const std::string& nomDoc, InfTermDoc& InfDoc)const;
    
    //Devuelve true si word aparece como termino indexado
    bool Existe(const std::string& word)const;
    
    //Devuelve true si se realiza el borrado p.ej si word aparece como termino indexado
    bool Borra(const std::string& word);
    
    /*Devuelve true si nomDoc esta indexado y se realiza el borrado de todos los 
     * terminos del documento y del docuemnto en los campos privados "indiceDocs" e
     * "informacionColeccionDocs"
     */
    bool BorraDoc(const std::string& nomDoc);
    
    //Borra todos los terminos del indice de la pregunta
    void VaciarIndiceDocs();
    
    //Borra todos los terminos del indice de la pregunta
    void VaciarIndicePreg();
    
    //sera true si word esta indexado, sustituyendo la informacion almacenada por "inf"
    bool Actualiza(const std::string& word, const InformacionTermino& inf);
    
    //Sera true si se realiza la insercion p.ej si word no estaba previamente indexado
    bool Inserta(const std::string& word, const InformacionTermino& inf);
    
    //Devolvera el numero de terminos diferentes indexados (cardinalidad de campo privado "indice")
    int NumPalIndexadas()const;
    
    //Devuelve el contenido del campo privado "ficheroStopWords"
    std::string DevolverFichPalParada()const;
    
    /*Mostrara por pantalla las paralbras de paraa almacenadas (originales, sin aplicar stemming):
     * una plabra por linea (salto de linea al final de cada palabra)
     */
    void ListarPalParada()const;
    
    //Devolvera el numero de palabras de parada almacenadas
    int NumPalParada() const;
    
    //Devuelve los delimitadores utilizados por el tokenizador
    std::string DevolverDelimitadores() const;
    
    //Devuelve si el tokenizador analiza los casos especiales
    bool DevolverCasosEspeciales () const;
    
    //Devuelve si el tokenizador pasa a minusculas y sin acentos
    bool DevolverPasarAminuscSinAcentos () const;
    
    //Devuelve el valor de almacenarPosTerm
    bool DevolverAlmacenarPosTerm() const;
    
    //Devuelve "directorioIndice" (el directorio del disco duro donde se almacenara el indice)
    std::string DevolverDirIndice() const;
    
    //Devolvera el tipo de stemming realizado en la indexcion de acuerdo con el valo de "sipoStemmer"
    int DevolverTipoStemming() const;
    
    //Devolvera el valor indicado en la variable privada "almEnDisco"
    bool DevolverAlmEnDisco() const;
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indice": cout << termino <<'\t' << InformacionTermino << endl;*/
    void ListarInfColeccDocs() const;
    
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla todos 
     * los terminos indexados del documento con nombre 
     * "nomDoc": cout << termino << '\t' << InformacionTermino << endl;
     * si no existe no se muestra nada*/
    bool ListarTerminos(const std::string& nomDoc)const;
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indiceDocs": cout<< nomDoc << '\t' << infDoc << endl;
     */
    void ListarDocs() const;
   
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla el contenido
     * del campo privado indiceDocs para el docuemnto con nombre 
     * "nomDoc": cout << nomDoc << '\t' << infDoc << endl;
     * si no existe no se muestra nada*/
    bool ListarDocs(const std::string& nomDoc) const;
    
    void file_to_stopWords(const std::string& );
    
    void copia(const IndexadorHash&);
    
    bool IsAlmacenarEnDisco() const {
        return almacenarEnDisco;
    }

    void SetAlmacenarEnDisco(bool almacenarEnDisco) {
        this->almacenarEnDisco = almacenarEnDisco;
    }

    bool IsAlmacenarPosTerm() const {
        return almacenarPosTerm;
    }

    void SetAlmacenarPosTerm(bool almacenarPosTerm) {
        this->almacenarPosTerm = almacenarPosTerm;
    }

    std::string GetDirectorioIndice() const {
        return directorioIndice;
    }

    void SetDirectorioIndice(std::string directorioIndice) {
        this->directorioIndice = directorioIndice;
    }

    std::string GetFicheroStopWords() const {
        return ficheroStopWords;
    }

    void SetFicheroStopWords(std::string ficheroStopWords) {
        this->ficheroStopWords = ficheroStopWords;
    }

    std::unordered_map<std::string, InformacionTermino> GetIndice() const {
        return indice;
    }

    void SetIndice(std::unordered_map<std::string, InformacionTermino> indice) {
        this->indice = indice;
    }

    std::unordered_map<std::string, InfDoc> GetIndiceDocs() const {
        return indiceDocs;
    }

    void SetIndiceDocs(std::unordered_map<std::string, InfDoc> indiceDocs) {
        this->indiceDocs = indiceDocs;
    }

    std::unordered_map<std::string, InformacionTerminoPregunta> GetIndicePregunta() const {
        return indicePregunta;
    }

    void SetIndicePregunta(std::unordered_map<std::string, InformacionTerminoPregunta> indicePregunta) {
        this->indicePregunta = indicePregunta;
    }

    InformacionPregunta GetInfPregunta() const {
        return infPregunta;
    }

    void SetInfPregunta(InformacionPregunta infPregunta) {
        this->infPregunta = infPregunta;
    }

    InfColeccionDocs GetInformacionColeccionDocs() const {
        return informacionColeccionDocs;
    }

    void SetInformacionColeccionDocs(InfColeccionDocs informacionColeccionDocs) {
        this->informacionColeccionDocs = informacionColeccionDocs;
    }

    std::string GetPregunta() const {
        return pregunta;
    }

    void SetPregunta(std::string pregunta) {
        this->pregunta = pregunta;
    }

    std::unordered_set<std::string> GetStopWords() const {
        return stopWords;
    }

    void SetStopWords(std::unordered_set<std::string> stopWords) {
        this->stopWords = stopWords;
    }

    int GetTipoStemmer() const {
        return tipoStemmer;
    }

    void SetTipoStemmer(int tipoStemmer) {
        this->tipoStemmer = tipoStemmer;
    }

    Tokenizador GetTok() const {
        return tok;
    }

    void SetTok(Tokenizador tok) {
        this->tok = tok;
    }

    bool IndexarDocumento(const std::string&);
    
    int CalculaTamFichero(const std::string& )const;
    Fecha FechaModFichero(const std::string& )const;
    
    void getDocName(const long int& id, std::string & name) const;
private:
    long int idDocPos;
    /*Este constructor se pone en la parte privada porque no se permitira crear un
     * indexador sin inicializarlo convenientemente. La inicializacion la decidira el alumno
     */
    IndexadorHash();
    
    //Indice de terminos indexados accesible por el termino
    std::unordered_map<std::string, InformacionTermino> indice;
    
    //Indice de documentos indexados accesible por el nombre del documento
    std::unordered_map<std::string, InfDoc> indiceDocs;
    
    //Informacion recogida de la colecci�n de documentos indexada
    InfColeccionDocs informacionColeccionDocs;
    
    // Pregunta indexada actualmente. Si no hay ninguna indexada, contendria el valor ""
    std::string pregunta;
    
    //indice de terminos indexados en una pregunta. Se almacenara en memoria principal
    std::unordered_map<std::string, InformacionTerminoPregunta> indicePregunta;
    
    //Informacion recogida de la pregunta indexada. se almacenara en memoria principal
    InformacionPregunta infPregunta;
    
    //palabras de parada. se almacenara en memoria principal
    std::unordered_set<std::string> stopWords;
    
    //nombre del fichero que contiene las palabras de parada
    std::string ficheroStopWords;
    
    /*tokenizador que se usara en la indexacion. se inicializara con los parametros del 
     * constructor: detectComp y minuscSinAcentos
     */
    Tokenizador tok;
    
    /*"directorioIndice" sera el directorio del disco duro donde se almacenara el indice.
     * en caso que contenga la cadena vacia se creara en el directorio donde se ejecuta 
     * el indexador*/
    std::string directorioIndice;
    
    /*0 = no se aplica stemmer, se indexa tal y como aparece tokenizado
     * 1 = stemmer de porter para espa�ol
     * 2 = stemmer de porter para ingles
     * para el stemmer de porte se utilizaran ficheros dados, sus funciones de nombre "stemmer"*/
    int tipoStemmer;
    
    /*Esta opcion esta ideada para poder indexar colecciones de documentos lo suficientemente 
     * grandes para que su indexacion no quepa en memoria, por lo que si es true  se almacenara
     * la minima parte de los indices de los documentos en memoria principal, p.ej solo la 
     * estructura "indice" para saber la palabras indexadas,guardando solo punteros 
     * a las posiciones de los archivos almacenados en disco que contengan el resto de 
     * informacion asociada a cada termino (lo mismo para indiceDocs). Para los datos 
     * de la indexacion de la pregunta no habria que hacer nada. En caso de que esta 
     * variable tenga valor false, se almacenara todo el indice en memoria principal
     * (tal y como se ha descrito anteriormente)*/
    bool almacenarEnDisco;
    
    /*si es true se almacenara la posicion en la que aparecen los erminos dentro del 
    * documento en la clase InfTermDoc*/
    bool almacenarPosTerm;
};


#endif /* INDEXADORHASH_H */

