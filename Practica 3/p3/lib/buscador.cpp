#include "../include/buscador.h"
#include <math.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
typedef std::unordered_map<std::string, InfDoc> tipo_indiceDocs;
typedef std::unordered_map<std::string, InformacionTerminoPregunta> tipo_indicePregunta;
typedef std::unordered_map<std::string, InformacionTermino> tipo_indice;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//ResultadoRI

ResultadoRI::ResultadoRI(const double& kvSimilitud, const int& kidDoc, const int& np){
    vSimilitud = kvSimilitud;
    idDoc = kidDoc;
    numPregunta = np;
}
double ResultadoRI::VSimilitud() const{
    return vSimilitud;
}
long int ResultadoRI::IdDoc() const{
    return idDoc;
}
int ResultadoRI::NumPregunta() const{
    return numPregunta;
}

bool ResultadoRI::operator <(const ResultadoRI& lhs) const{
    if(numPregunta == lhs.numPregunta){
        return vSimilitud < lhs.vSimilitud;
    }
    else return numPregunta > lhs.numPregunta;
}

std::ostream& operator<<(std::ostream & os, const ResultadoRI &res){
    os << res.VSimilitud() << "\t\t" << res.IdDoc() << "\t" << res.NumPregunta() << "\n";
    
    return os;
}



//Buscador 
std::ostream& operator<<(std::ostream& s, const Buscador& p){
    std::string preg;
    s << "Buscador: " << "\n";
    if(p.DevuelvePregunta(preg)){
        s << "\tPregunta indexada: " << preg << "\n";
    }else{
        s << "No hay ninguna pregunta indexada" << "\n";
    }
    s << "\tDatos del indexador: " << "\n" << (IndexadorHash) p;
    //invoca a la sobrecarga de la salida del IndexadorHash
    return s;
}
Buscador::Buscador(): IndexadorHash(""){

}

Buscador::Buscador(const std::string& directorioIndexacion, const int& f) : IndexadorHash(directorioIndexacion){
    
    formSimilitud = f;
    c = 2;
    k1 = 1.2;
    b = 0.75;

}

Buscador::Buscador(const Buscador& buscador) : IndexadorHash(buscador){
    Buscador::Copia(buscador);
}

void Buscador::Copia(const Buscador& buscador){
    
    this->formSimilitud = buscador.formSimilitud;
    this->c = buscador.c;
    this->k1 = buscador.k1;
    this->b = buscador.b;
    
}

Buscador::~Buscador(){
    while(!docsOrdenados.empty()){
            docsOrdenados.pop();
        }
    this->formSimilitud = 0;
    this->c = 0;
    this->k1 = 0;
    this->b = 0;
}

Buscador& Buscador::operator =(const Buscador& buscador){
    if(this != &buscador){
            //(*this).~Buscador();
            Buscador::copia(buscador);
        }
        return *this;
}

bool Buscador::buscarnpreg(const int& numDocumentos,const int& num){
    
    std::priority_queue<ResultadoRI> aux;
    std::string preg;
    if(DevuelvePregunta(preg)){
       
        //vaciamos el contenido de docsOrdenado
        if(num == 0){
            while(!docsOrdenados.empty()){
                docsOrdenados.pop();
            }
        }
        
        
        //vamos a realizar la b�squeda
        if(formSimilitud == 0){
            //formSimilitud es 0, DFR
            //inftermdoc auxiliar
            InfTermDoc itdaux;
            
            //Cantidad de documentos de la coleccion;
            int N = GetInformacionColeccionDocs().getNumDocs();
            double avr_ld = GetInformacionColeccionDocs().getTamBytes() / N;
            //la longitud en bytes del documento
            double ld;
            //el docuemtno de que se calcula su valor de similitud simq
            long int d;
            //el numero de documentos en los queaparece el termino t
            long int nt;
            //el numero de veces que el termino t aparece en la query
            int ftq;
            //el numero de veces que el termino aparece en el documento
            int ftd;
            //numero total de veces que aparece el termino el la coleccion
            int ft;
            //peso en la query del termino i, peso en el documento del termino i
            double wiq, wid;
            //numero de terminos de la query q
            int k = GetInfPregunta().getNumTotalPalSinParada();
            //similitud
            double sim;
            double ftdEstrella;
            double lambda;
            
            
            tipo_indice indice = GetIndice();
            tipo_indiceDocs indiceDocs = GetIndiceDocs();
            for (tipo_indiceDocs::const_iterator iterator = indiceDocs.begin(); iterator != indiceDocs.end(); ++iterator) {
                ld = iterator->second.GetTamBytes();
                d = iterator->second.GetIdDoc();
                sim = 0;
                //para cada documento iteramos por cada termino de la pregunta
                tipo_indicePregunta indicePregunta = GetIndicePregunta();
                for (tipo_indicePregunta::const_iterator iterator2 = indicePregunta.begin(); iterator2 != indicePregunta.end(); ++iterator2) {
                    ftq = iterator2->second.getFt();
                    tipo_indice::iterator indicepal = indice.find(iterator2->first);
                    if (indicepal != indice.end()){
                        //numero total de veces que aparece el termino el la coleccion
                        ft = indicepal->second.getFtc();
                        
                        nt = indicepal->second.getL_docs().size();
                        
                        
                        //recupero la cantidad de veces que aparece el termino en el doc
                        if (nt > 0 && indicepal->second.devuelveInfTermDoc(d, itdaux)){
                            ftd = itdaux.getFt();
                        }else { ftd = 0; }
                        //ya podemos hacer el calculo

                        ftdEstrella =ftd * log2(1 + c * avr_ld / ld);  
                        wiq = ftq / k;
                        lambda = ft / N;
                        wid = (log2(1 + lambda) + ftdEstrella * log2((1 + lambda) / lambda))
                                * (ft + 1 / (nt * (ftdEstrella + 1)));
                        if(std::isnan(wid)){wid = 0;} if(std::isnan(wiq)){wiq= 0;}
                        sim += wid * wiq;
                        
                    } else {
                    //da 0 de resultado
                    }
                    
                } 

                aux.push(ResultadoRI(sim, d, num));
                
            }
            
            
            
            
            
            
        }
        else{
            //formSimilitud es 1, BM25
            
            //inftermdoc auxiliar
            InfTermDoc itdaux;
            //numero de terminos de la query
            int n = GetInfPregunta().getNumTotalPalSinParada();
            long int avgdl = GetInformacionColeccionDocs().getNumTotalPalSinParada() /
                             GetInformacionColeccionDocs().getNumDocs();

            //Cantidad de documentos de la coleccion;
            int N = GetInformacionColeccionDocs().getNumDocs();
            //numero de palabras no de parada del documento D
            int D = 0;
            double scoreDQ;
            //numero de documentos en los que aparece el termino qi
            int nqi;
            //la frecuencia del termino en el documento
            int fqd;
            
            //el id del doc
            long int idDoc;
            tipo_indice indice = GetIndice();
            //Iteramos sobre todos los documentos para realizar los calculos para cada uno de ellos
            tipo_indiceDocs indiceDocs = GetIndiceDocs();
            for (tipo_indiceDocs::const_iterator iterator = indiceDocs.begin(); iterator != indiceDocs.end(); ++iterator) {
                D = iterator->second.GetNumPalSinParada();
                idDoc = iterator->second.GetIdDoc();
                scoreDQ = 0;
                //para cada documento iteramos por cada termino de la pregunta
                tipo_indicePregunta indicePregunta = GetIndicePregunta();
                for (tipo_indicePregunta::const_iterator iterator2 = indicePregunta.begin(); iterator2 != indicePregunta.end(); ++iterator2) {
         
                    tipo_indice::iterator indicepal = indice.find(iterator2->first);
                    if (indicepal != indice.end()){
                        nqi = indicepal->second.getL_docs().size();
                        //recupero la cantidad de veces que aparece el termino en el doc
                        if (nqi > 0 && indicepal->second.devuelveInfTermDoc(idDoc, itdaux)){
                            fqd = itdaux.getFt();
                        }else { fqd = 0; }
                        //ya podemos hacer el calculo

                        double scoreDQaux = log( (N - nqi + 0.5) / nqi + 0.5) *
                                fqd + k1 * (1 - b + b * (D/avgdl));
                        if(std::isnan(scoreDQaux)){ scoreDQaux = 0;}
                        scoreDQ += scoreDQaux;
                    } else {}
                                        
                } 

                aux.push(ResultadoRI(scoreDQ, idDoc, num));
                
            }
            
        }
        unsigned int nDocs = numDocumentos;
        if (numDocumentos > aux.size()) nDocs = aux.size();
    
        for(unsigned int i = 0; i < nDocs; ++i  ){
            docsOrdenados.push(aux.top());
            aux.pop();
        }
    }
    else return false;
}

bool Buscador::Buscar(const int& numDocumentos){
    return buscarnpreg(numDocumentos, 0);
}

bool Buscador::Buscar(const std::string& dirPreguntas, const int& numDocumentos, 
            const int& numPregInicio, const int& numPregFin){
    struct stat dir;
        
    int err=stat(dirPreguntas.c_str(), &dir);
    if(err == -1 ||!S_ISDIR(dir.st_mode)){
        std::cerr <<"error con el directorio" << dirPreguntas;
        return false;
    }else{
        std::string cmd = "find "+dirPreguntas+" -L -mindepth 1 |sort > .lista_fich";
        system(cmd.c_str());
        //return TokenizarListaFicheros(".lista_fich");
        
        std::string lista = ".lista_fich";
        std::string line;
        
        std::ifstream ifs;
        ifs.open(lista.c_str());
        int num;
        std::string rawname;
        std:: string pregunt ="";
        while (getline(ifs, line)) {
            if (line.size () > 3){
                size_t lastindex = line.find_last_of("."); 
                rawname = line.substr(0, lastindex);
            }
            num = stoi(line);
            if(num >= numPregInicio || num <= numPregFin){
                //ahora abrimos rawname y llamamos a buscar con el numero de pregunta 
                std::ifstream fichpreg;
                fichpreg.open("dirPreguntas/" + line);
                getline(fichpreg, pregunt);
                IndexarPregunta(pregunt);

                buscarnpreg(numDocumentos, num);
                fichpreg.close();
                
                
            }
        }
        ifs.close();
        return true;
    } 
}


void Buscador::ImprimirResultadoBusqueda(const int& numDocumentos) const{
    std::priority_queue<ResultadoRI> docc ;
    docc = docsOrdenados ;
    unsigned int nDocs = numDocumentos;
    if (numDocumentos > docsOrdenados.size()) nDocs = docsOrdenados.size();
    std::string tipos = "";
    std::string nomdoc;
    Tokenizador tok;
    tok.setDelimiters("/");
    std::vector<std::string> tokens;
    
    for(unsigned int i = 0; i < nDocs; ++i  ){
        if (docc.top().VSimilitud() == 0){
            tipos = "DFR";
        }else{tipos = "BM25";}
        
        
                
        getDocName(docc.top().IdDoc(), nomdoc );
        tok.Tokenizar (nomdoc , tokens );

        if(!tokens.empty()){
            nomdoc = tokens.back();
        }
        size_t lastindex = nomdoc.find_last_of("."); 
        nomdoc = nomdoc.substr(0, lastindex);
        //std::cout << docc.top();
        std::cout << docc.top().NumPregunta()<<" "<< tipos << " " << nomdoc 
                << " " << docc.top().VSimilitud() << " ";
        if(docc.top().NumPregunta() == 0){
            std::cout << GetPregunta() << " ";
        }else {
            std::cout << "ConjuntoDePreguntas ";
        }
        std::cout<<"\n";
        docc.pop();
    }
}
bool Buscador::ImprimirResultadoBusqueda(const int& numDocumentos, const std::string& nombreFichero) const{
    std::ofstream i; 
    i.open(nombreFichero);
    if(!i){
        //si no existe se crea
        //i.open(nombreFichero,  std::fstream::out | std::fstream::trunc);
        std::cerr<< "Error al crear el fichero " << nombreFichero;
        return false;
    }
    std::stringstream res;
    
    
    std::streambuf *inicial = std::cout.rdbuf();
    std::cout.rdbuf(i.rdbuf());
    ImprimirResultadoBusqueda(numDocumentos);
    std::cout.rdbuf(inicial);
    ImprimirResultadoBusqueda(numDocumentos);

    i.close();
    return true;
}



int Buscador::DevolverFormulaSimilitud() const{
    return this->formSimilitud;
}

bool Buscador::CambiarFormulaSimilitud(const int& f){
    if(f == 1 || f == 0){
        formSimilitud = f;
        return true;
    }
    else return false;
}

void Buscador::CambiarParametrosDFR(const double& kc){
    this->c = kc;
}

double Buscador::DevolverParametrosDFR() const{
    return c;
}

void Buscador::CambiarParametrosBM25(const double& kk1, const double& kb){
    this->k1 = kk1;
    this->b = kb;
}

void Buscador::DevolverParametrosBM25(double& kk1, double& kb) const{
    kk1 = this->k1;
    kb = this->b;
}