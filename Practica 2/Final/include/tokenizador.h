#ifndef TOKENIZADOR_H
#define TOKENIZADOR_H
#include <iostream>
#include <unordered_set>
#include <set>
#include <unordered_map>
using namespace std;

class Tokenizador {
    friend ostream& operator<<(ostream&, const Tokenizador&);
    /* cout << "DELIMITADORES: " << delimiters << " TRATA CASOS ESPECIALES: "
    * << casosEspeciales << " PASAR A MINUSCULAS Y SIN ACENTOS: " <<
    * pasarAminuscSinAcentos;
    * Aunque se modifique el almacenamiento de los delimiters por temas 
    * de eficiencia, el campo delimiters se imprimira con el string con el que
    * se inicializo el toquenizador
    */
public: 
    Tokenizador (const Tokenizador&);
    
    Tokenizador (const string&, const bool& casosEspeciales, const bool& minuscSinAcentos);
    /* Inicializa variable privada delimiters a delimitadoresPalabra
    * casosEspeciales a casosEspeciales y pasarAminuscSinAcentos a minuscSinAcentos
    */
    Tokenizador();
    /* Inicializa delimiters =",;:.-/+*\\'\"{}[]()<>�!�?&#=\t\n\r@"
    * casosEspeciales a true y pasarAminuscSinAcentos a false
    */
    ~Tokenizador (); //Pone delimiters=""
    
    void copia(const Tokenizador&);
    
    Tokenizador& operator= (const Tokenizador&);
    
    
    void Tokenizar (const string& , list<string>& )const ;
    // Tokeniza str devolviendo el resultado en tokens. La lista tokens se vaciar� antes de almacenar el resultado de la tokenizaci�n.
    
    bool Tokenizar (const string& , const string& )const;
    /*Tokeniza el fichero i guardando la salida en el fichero f
    (una palabra en cada linea del fichero). Devolver� true si se realiza la tokenizacion
     * de forma correcta enviando a cerr el mensaje correspondiente (p.ej que no exista el archivo i)*/
    
    bool Tokenizar (const string & ) const;
   /*Tokeniza el fichero i guardando la salida en un fichero de nombre i
     a�adiendole la extension .tk (sin eliminar previamente la extension de i por ejemplo,
     * del archivo pp.text se generaria el resultado en pp.txt.tk),
     y que contendr� una palabra en cada linea del fichero. Devlver� true si
     se realiza la tokenizacion de forma correcta enviando a cerr el mensaje 
     * correspondiente (p.ej. que no exista el archivo i)*/
    
    bool TokenizarListaFicheros (const string& ) const;
    /* Tokeniza el fichero i que contiene un nombre de fichero por l�nea 
     guardando la salida en ficheros (uno por cada linea de i) cuyo nombre
     sera el leido en i a�adiendole la extension .tk, y que contendra una 
     * palabra en cada linea del fichero leido en i.Devolvera true si se 
     * realiza la tokenizacion de forma correcta de todos los archivos que
     contiene i: devolvera false en caso contrario enviando acerr el mensaje
     correspondiente (p.ej. que no exista el archivo i, o bien enviando a
     * "cerr" los archivos de i que no existan, luego no se ha de interrumpir
     * la ejecucion si hay algun archivo en i que no exista)*/
    bool TokenizarDirectorio (const string& ) const;
    /* Tokeniza todos los archivos que contenga el directorio i, incluyendo
     los de los subdirectorios, guardando la salida en ficheros cuyo nombre
     sera el de entrada a�adiendole la extension .tk, y que contendra una
     palabra en cada linea del fichero. Devolvera true si se realiza la 
     tokenizacion de forma correcta de toos los archivos; devolvera false en
     caso contrario enviando a cerr el mensaje correspondiente (p.ej. que no
     * exista el directorio i, o los ficheros que no se hayan podido tokenizar)*/
    
    void DelimitadoresPalabra(const string& );
    // Cambia delimiters por "nuevoDelimiters"
    
    void AnyadirDelimitadoresPalabra(const string& );
    /* A�ade al final de delimiters los nuevos delimitadores que aparezcan
     * en "nuevoDelimiters" (no se almacenaran caracteres repetidos)
     */
    string AddDelimitadoresPalabraToString(const string& );
    string DeleteFromSpecialCaseDelimiters(const string& );
    //void FillDelimitersSet();
    void ToLowerWithoutAccent(string& )const;
    void ToLowerWithoutAccent1(string& )const;
    string DelimitadoresPalabra() const;
    //devuelve "Delimiters"
    
    void CasosEspeciales (const bool& nuevoCasosEspeciales);
    // Cambian la variable privada "casosEspeciales"

    bool CasosEspeciales()const;
    // Devuelve el contenido de la variable privada "casosEspeciales"

    void PasarAminuscSinAcentos (const bool& nuevoPasarAminuscSinAcentos);
    /*
    * Cambia la variable privada pasarAminuscSinAcentos. Atencio al 
    * formato de codificacion del corpus (comando "file" de linux) para la
    * correccion de la practica se utilizara el formato actual ISO-8859
    */

    bool PasarAminuscSinAcentos()const;
    // devuelve el contenido de la variable privada "pasarAminuscSinAcentos"
    
    bool isCasosEspeciales() const {
        return casosEspeciales;
    }

    void setCasosEspeciales(bool casosEspeciales) {
        this->casosEspeciales = casosEspeciales;
    }

    string getDelimiters() const {
        return delimiters;
    }

    void setDelimiters(string delimiters) {
        this->delimiters = delimiters;
    }

    bool isPasarAminuscSinAcentos() const {
        return pasarAminuscSinAcentos;
    }

    void setPasarAminuscSinAcentos(bool pasarAminuscSinAcentos) {
        this->pasarAminuscSinAcentos = pasarAminuscSinAcentos;
    }

private:
    string delimiters;
    /* Delimitadores de terminos. Aunque se modifique la forma de
     * almacenamiento interna para mejorar la eficiencia, este campo debe
     * permanecer para indicar el orden en que se introdujeron los
     * delimitadores
     */

    bool casosEspeciales;
    //si true detectara palabras compuestas y casos especiales

    bool pasarAminuscSinAcentos;
    /*
    * si true pasara el token a minusculas y quitar� acentos, antes de 
    * realizar la toquenizacion
    */
    
    string specialDelimitersWithoutURL;
    string specialDelimitersWithoutEmail;
    string specialDelimitersWithoutNumbers;
    string specialCaseDelimiters;
    //unordered_set<char> delimitersSet;
//    const unordered_map<int, int> accentToLowerNotAccent =
//    {   //upper A with accent
//        {-64, 97}, {-63, 97}, {-62, 97}, {-61, 97}, {-60, 97}, {-59, 97},
//        //AE  �
//        {-58, -26}, {-57, -25},
//        //upper E with accent
//        {-56, 101}, {-55, 101}, {-54, 101}, {-53, 101},
//        //Upper I whit accent
//        {-52, 105}, {-51, 105}, {-50, 105}, {-49, 105},
//        //delta �
//        {-48, -16}, {-47, -15}, 
//        //Upper O with accent
//        {-46, 111}, {-45, 111}, {-44, 111}, {-43, 111}, {-42, 111},
//        //Upper U with accent
//        {-39, 117}, {-38, 117}, {-37, 117}, {-36, 117},
//        //Upper Y with accent ro, 
//        {-35, 121}, {-34, -2}, 
//        
//        //lower A with accent
//        {-32, 97}, {-31, 97}, {-30, 97}, {-29, 97}, {-28, 97}, {-27, 97}, 
//        //lower E with accent
//        {-24, 101}, {-23, 101}, {-22, 101}, {-21, 101},
//        //lower I whit accent
//        {-20, 105}, {-19, 105}, {-18, 105}, {-17, 105},
//        //lower O with accent
//        {-14, 111}, {-13, 111}, {-12, 111}, {-11, 111}, {-10, 111},
//        //lower U with accent
//        {-7, 117}, {-6, 117}, {-5, 117}, {-4, 117},
//        //lower Y with accent 
//        {-3, 121}, {-1, 121},
//    
//    };
};

#endif