#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
#include <sys/stat.h>
#include <cstdlib>
#include <ctype.h>

using namespace std;
#include "../include/tokenizador.h"
    static const string URL = "_:/.?&-=#@";
    static const string numbers = "%$?���";
    static const string email = "._-@";
    static  unordered_map<int, int> accentToLowerNotAccent =
    {   //upper A with accent
        {-64, -159}, {-63, -159}, {-62, -159}, {-61, -159}, {-60, -159}, {-59, -159},
        //AE  �
        {-58, -26}, {-57, -25},
        //upper E with accent
        {-56, -155}, {-55, -155}, {-54, -155}, {-53, -155},
        //Upper I whit accent
        {-52, -151}, {-51, -151}, {-50, -151}, {-49, -151},
        //delta �
        {-48, -16}, {-47, -15}, 
        //Upper O with accent
        {-46, -145}, {-45, -145}, {-44, -145}, {-43, -145}, {-42, -145},
        //Upper U with accent
        {-39, -139}, {-38, -139}, {-37, -139}, {-36, -139},
        //Upper Y with accent ro, 
        {-35, -135}, {-34, -2}, 
        
        //lower A with accent
        {-32, -159}, {-31, -159}, {-30, -159}, {-29, -159}, {-28, -159}, {-27, -159}, 
        //lower E with accent
        {-24, -155}, {-23, -155}, {-22, -155}, {-21, -155},
        //lower I whit accent
        {-20, -151}, {-19, -151}, {-18, -151}, {-17, -151},
        //lower O with accent
        {-14, -145}, {-13, -145}, {-12, -145}, {-11, -145}, {-10, -145},
        //lower U with accent
        {-7, -139}, {-6, -139}, {-5, -139}, {-4, -139},
        //lower Y with accent 
        {-3, -135}, {-1, -135},
    
    };
    

    ostream& operator<<(ostream &s, const Tokenizador &obj){
        s << "DELIMITADORES: " << obj.delimiters << " TRATA CASOS ESPECIALES: "
     << obj.casosEspeciales << " PASAR A MINUSCULAS Y SIN ACENTOS: " <<
     obj.pasarAminuscSinAcentos;
    }
    /* cout << "DELIMITADORES: " << delimiters << " TRATA CASOS ESPECIALES: "
    * << casosEspeciales << " PASAR A MINUSCULAS Y SIN ACENTOS: " <<
    * pasarAminuscSinAcentos;
    */

    Tokenizador::Tokenizador (const Tokenizador& tokenObj){
        copia(tokenObj);
    }
    
    Tokenizador::Tokenizador (const string& delimitadoresPalabra, const bool& kcasosEspeciales, const bool& minuscSinAcentos){
        delimiters = delimitadoresPalabra;
        specialCaseDelimiters = AddDelimitadoresPalabraToString(" ");
        specialDelimitersWithoutURL = DeleteFromSpecialCaseDelimiters(URL);
        specialDelimitersWithoutEmail = DeleteFromSpecialCaseDelimiters(email);
        specialDelimitersWithoutNumbers = DeleteFromSpecialCaseDelimiters(numbers);
        casosEspeciales = kcasosEspeciales;
        pasarAminuscSinAcentos = minuscSinAcentos;
        //FillDelimitersSet();
    }
    /* Inicializa variable privada delimiters a delimitadoresPalabra
    * casosEspeciales a kcasosEspeciales y pasarAminuscSinAcentos a minuscSinAcentos
    */
    Tokenizador::Tokenizador(){
        delimiters = ",;:.-/+*\\ '\"{}[]()<>�!�?&#=\t\n\r@";
        //because delimiters already has the " "
        specialCaseDelimiters = delimiters;
        specialDelimitersWithoutURL = DeleteFromSpecialCaseDelimiters(URL);
        specialDelimitersWithoutEmail = DeleteFromSpecialCaseDelimiters(email);
        specialDelimitersWithoutNumbers = DeleteFromSpecialCaseDelimiters(numbers);
        casosEspeciales = true;
        pasarAminuscSinAcentos = false;
        //FillDelimitersSet();
    }
    /* Inicializa delimiters =",;:.-/+*\\ '\"{}[]()<>?!??&#=\t\n\r@"
    * casosEspeciales a true y pasarAminuscSinAcentos a false
    */
    Tokenizador::~Tokenizador (){
        delimiters = "";
        //delimitersSet.clear();
    } //Pone delimiters=""
    
    void Tokenizador::copia(const Tokenizador&tokenObj){
        delimiters = tokenObj.delimiters;
        casosEspeciales = tokenObj.casosEspeciales;
        pasarAminuscSinAcentos = tokenObj.pasarAminuscSinAcentos;
        specialCaseDelimiters = tokenObj.specialCaseDelimiters;
        specialDelimitersWithoutURL = tokenObj.specialDelimitersWithoutURL;
        specialDelimitersWithoutEmail = tokenObj.specialDelimitersWithoutEmail;
        specialDelimitersWithoutNumbers = tokenObj.specialDelimitersWithoutNumbers;
        //delimitersSet = tokenObj.delimitersSet;
    }
    
    Tokenizador& Tokenizador::operator= (const Tokenizador& tokenObj){
        if(this != &tokenObj){
            //(*this).~Tokenizador();
            copia(tokenObj);
        }
        return *this;
    }

    void Tokenizador::Tokenizar (const string& s, list<string>&  tokens) const{
        
        tokens.clear();
        
        if(casosEspeciales){
            string str =  s;
            str += " ";
            if(pasarAminuscSinAcentos){
                ToLowerWithoutAccent(str);
            }
            bool processed;
            char delim;

            string::size_type firstPos = str.find_first_not_of(specialCaseDelimiters, 0);
            string::size_type delimPos = str.find_first_of(specialCaseDelimiters, firstPos);
            //this while doesnt process the last token
            while(string::npos != delimPos ){

                delim = str.at(delimPos);
                processed = false;
                //1� URL **********************************************************************************************
                int startofAddressURL = 0;
                if(str.size() > (firstPos + 4) && str.at(firstPos) == 'f' && str.at(firstPos + 1) == 't' && str.at(firstPos + 2) == 'p' && str.at(firstPos + 3) == ':' ){
                    //es un ftp
                    
                    startofAddressURL = firstPos + 4;
                    if(specialDelimitersWithoutURL.find(str.at(startofAddressURL)) == string::npos){
                        //es una url
                        delimPos = str.find_first_of(specialDelimitersWithoutURL,startofAddressURL +1);
                        tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                        processed = true;
                    }
                }
                else if(str.size() > (firstPos + 5) && str.at(firstPos) == 'h' && str.at(firstPos + 1) == 't' && str.at(firstPos +2) == 't' &&  str.at(firstPos + 3) == 'p'){
                    if(str.at(firstPos + 4) == ':'){
                        //es un http

                            startofAddressURL = firstPos + 5;
                        
                    }
                    else if(str.size() > (firstPos + 6) && str.at(firstPos + 4) == 's' && str.at(firstPos + 5) == ':'){
                        //es un https
                        startofAddressURL = firstPos + 6;
                    }
                    if(startofAddressURL > 0 && specialDelimitersWithoutURL.find(str.at(startofAddressURL)) == string::npos){
                        //es una url
                        delimPos = str.find_first_of(specialDelimitersWithoutURL,startofAddressURL);
                        tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                        processed = true;
                    }
                }
                //2� numbers ***************************************************************************************
                else if(!processed){
                    
                    bool checked = false;
                    string::size_type endOfPossibleNumber = delimPos;
                    string::size_type begginingAux = firstPos;
                    char delimAux = delim;
                    while (!checked){
                        unsigned int i = begginingAux;
                        //check the number but the last char
                        for(;i < endOfPossibleNumber - 1; i ++){
                            if (!isdigit(str[i])){
                                //check if is a dot or comma
                                if(str[i] == ',' || str[i] == '.'){
                                    if(str[i + 1] == ',' || str[i + 1] == '.'){
                                        //two continous symbols is not a number
                                        checked = true;
                                        break;
                                    }
                                }else{
                                    //is not a number
                                    checked = true;
                                    break;
                                }
                            }
                        }
                        if(!checked){
          
                            //check the last char
                            if(numbers.find(str[i]) != string::npos){
                                //we have found a number with a symbol at the end
                                //save the number and symbol
                                checked = true;
                                delimPos = endOfPossibleNumber;
                                string toSave;
                                if(str[firstPos] == ',' || str[firstPos] == '.'){
                                    toSave = "0";
                                    toSave += str.substr(firstPos, endOfPossibleNumber - (firstPos + 1));
                                }
                                else if(firstPos > 0 &&(str[firstPos -1] == ',' || str[firstPos-1] == '.')){
                                    if (firstPos > 1 &&(str[firstPos -2] == ',' || str[firstPos-2] == '.')){
                                        toSave = str.substr(firstPos, endOfPossibleNumber - (firstPos + 1));
                                    }else{
                                        toSave = "0";
                                        toSave += str[firstPos -1];
                                        toSave += str.substr(firstPos, endOfPossibleNumber - (firstPos + 1));
                                    }     
                                }else{
                                    toSave = str.substr(firstPos, endOfPossibleNumber - (firstPos + 1));
                                }
                                tokens.push_back(toSave);
                                tokens.push_back(str.substr(endOfPossibleNumber - 1,1));
                                processed = true; 
                                
                            }else if(isdigit(str[i])){
                                if(delimAux == ',' || delimAux == '.'){
                                    //check if there is continuation of the number
                                    delimPos = endOfPossibleNumber;
                                    begginingAux = endOfPossibleNumber + 1;
                                    endOfPossibleNumber = str.find_first_of(specialCaseDelimiters, begginingAux);
                                    delimAux = str[endOfPossibleNumber];
                                    
                                }else{
                                    //save the number
                                    checked = true;
                                    delimPos = endOfPossibleNumber;
                                    string toSave;
                                    if(str[firstPos] == ',' || str[firstPos] == '.'){
                                        toSave = "0";
                                        toSave += str.substr(firstPos, endOfPossibleNumber - firstPos );
                                    }else if(firstPos > 0 &&(str[firstPos -1] == ',' || str[firstPos-1] == '.')){
                                        if (firstPos > 1 &&(str[firstPos -2] == ',' || str[firstPos-2] == '.')){
                                            toSave = str.substr(firstPos, endOfPossibleNumber - firstPos );
                                        }else{
                                            toSave = "0";
                                            toSave += str[firstPos -1];
                                            toSave += str.substr(firstPos, endOfPossibleNumber - firstPos );
                                        }
                                        
                                    }else{
                                        toSave = str.substr(firstPos, endOfPossibleNumber - firstPos );
                                    }
                                    tokens.push_back(toSave);
                                    //tokens.push_back(str.substr(firstPos, endOfPossibleNumber - firstPos ));
                                    processed = true;
                                }
                            }else checked = true;
                        }
                        
                        
                    }
               
                }
                
                //3� e-mails ****************************************************************************************
                if(!processed){
                    if (delim == '@' || delim == '.' || delim == '_' || delim == '-'){
                        unsigned int numberOfAt = 0;
                        string::size_type endOfPossibleEmail = str.find_first_of(specialDelimitersWithoutEmail,delimPos + 1);
                        if (endOfPossibleEmail == string::npos){
                            endOfPossibleEmail = str.size();
                        }
                        for (unsigned int i = firstPos; i < endOfPossibleEmail -1 && numberOfAt < 2; i ++){
                            if(str[i] == '@'){
                                numberOfAt ++;
                            }
                        }
                        if(numberOfAt == 1){
                            delimPos = endOfPossibleEmail;
                                tokens.push_back(str.substr(firstPos, endOfPossibleEmail - firstPos));
                                processed = true;
                        }

                    }  
                }

                //4� acronym
                if(!processed){
                    string::size_type endOfPossibleAcronym;
                    bool isAcronim = false;
                    while(delim == '.'){
                        endOfPossibleAcronym = str.find_first_of(specialCaseDelimiters,delimPos + 1);
                        if( (endOfPossibleAcronym - 1) == delimPos){
                            tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                            processed = true;
                            break;

                        }else if(endOfPossibleAcronym == string::npos){
                            tokens.push_back(str.substr(firstPos, str.size() - firstPos));
                            delimPos = endOfPossibleAcronym;
                            processed = true;
                            break;
                        }
                        else{
                            if(endOfPossibleAcronym -1 > delimPos){
                                isAcronim = true;
                            }
                            delimPos = endOfPossibleAcronym;
                            delim = str.at(delimPos);
                        }   
                    }
                    if (!processed && !isAcronim){
                        
                        unsigned int i;
                        for( i = firstPos; i < delimPos - 1; i++){
                            if(str[i] == '.'){
                                if(str[i] == str[i + 1]){
                                    isAcronim = false;
                                    break;
                                }else if(i > firstPos){
                                    isAcronim = true;
                                }
                            }
                            
                        }
                        if(isAcronim){
                            if(str[firstPos] == '.'){
                                firstPos += 1;
                            }
                            string::size_type aux = delimPos;
                            if(str[delimPos -1] == '.'){
                                aux -= 1;
                            }
                            tokens.push_back(str.substr(firstPos, aux - firstPos));
                            processed = true;
                        }
                    }
                }
                //5� hyphen *****************************************************************************
                if(!processed){
                    string::size_type endOfPossibleAcronym;
                    bool isAcronim = false;
                    while(delim == '-'){
                        endOfPossibleAcronym = str.find_first_of(specialCaseDelimiters,delimPos + 1);
                        if( (endOfPossibleAcronym - 1) == delimPos){
                            tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                            processed = true;
                            break;

                        }else if(endOfPossibleAcronym == string::npos){
                            tokens.push_back(str.substr(firstPos, str.size() - firstPos));
                            delimPos = endOfPossibleAcronym;
                            processed = true;
                            break;
                        }
                        else{
                            if(endOfPossibleAcronym -1 > delimPos){
                                isAcronim = true;
                            }
                            delimPos = endOfPossibleAcronym;
                            delim = str.at(delimPos);
                        }   
                    }
                    if (!processed && !isAcronim){
                        
                        unsigned int i;
                        for( i = firstPos; i < delimPos - 1; i++){
                            if(str[i] == '-'){
                                if(str[i] == str[i + 1]){
                                    isAcronim = false;
                                    break;
                                }else if(i > firstPos){
                                    isAcronim = true;
                                }
                            }
                        }
                        if(isAcronim){
                            
                            string::size_type aux = delimPos;
                            if(str[delimPos -1] == '-'){
                                aux -= 1;
                            }
                            tokens.push_back(str.substr(firstPos, aux - firstPos));
                            processed = true;
                        }
                    }
                }    

                
                //save regular tokenization
                    if(!processed){
                        tokens.push_back(str.substr(firstPos, delimPos - firstPos));
                    } 
                //before loop
                firstPos = str.find_first_not_of(specialCaseDelimiters, delimPos);
                delimPos = str.find_first_of(specialCaseDelimiters, firstPos);   
            }

        }else{
            string str = s;
            if(pasarAminuscSinAcentos){
                ToLowerWithoutAccent(str);
            }
            string::size_type lastPos = str.find_first_not_of(delimiters, 0);
            string::size_type pos = str.find_first_of(delimiters,lastPos);

            while(string::npos != pos || string::npos != lastPos){
                tokens.push_back(str.substr(lastPos, pos - lastPos));
                lastPos = str.find_first_not_of(delimiters, pos);
                pos = str.find_first_of(delimiters, lastPos);
            }

        }
        
    }
    
    // Tokeniza str devolviendo el resultado en tokens. La lista tokens se vaciar?antes de almacenar el resultado de la tokenizaci?.
    
    bool Tokenizador::Tokenizar (const string& fichEntr, const string& fichSal)const{
        ifstream i;
        ofstream f;
        string cadena;
        list<string> tokens;
        
        i.open(fichEntr);
        f.open(fichSal);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << fichEntr << "\n";
            return false;
        }else{
            list<string>::iterator itS;
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    Tokenizar(cadena,tokens);
                    for(itS = tokens.begin(); itS!= tokens.end(); itS++){
                        f << (*itS) << "\n";
                    }
                }
            }
        }
        i.close();
        
        
        
        f.close();
        return true;
        
    }
    /*Tokeniza el fichero i guardando la salida en el fichero f
    (una palabra en cada linea del fichero). Devolver?true si se realiza la tokenizacion
     * de forma correcta enviando a cerr el mensaje correspondiente (p.ej que no exista el archivo i)*/
    
    bool Tokenizador::Tokenizar (const string & i) const{
       string f = i + ".tk";
       return Tokenizar(i,f);
   }
   /*Tokeniza el fichero i guardando la salida en un fichero de nombre i
     a?diendole la extension .tk (sin eliminar previamente la extension de i por ejemplo,
     * del archivo pp.text se generaria el resultado en pp.txt.tk),
     y que contendr?una palabra en cada linea del fichero. Devlver?true si
     se realiza la tokenizacion de forma correcta enviando a cerr el mensaje 
     * correspondiente (p.ej. que no exista el archivo i)*/
    
    bool Tokenizador::TokenizarListaFicheros (const string& i) const{
        string line;
        bool allExist = true;
        ifstream ifs;
        ifs.open(i.c_str());
        while (getline(ifs, line)) {
            if(!Tokenizar(line)){
                allExist = false;
            }
        }
        ifs.close();
        return allExist;
    }
    /* Tokeniza el fichero i que contiene un nombre de fichero por l?ea 
     guardando la salida en ficheros (uno por cada linea de i) cuyo nombre
     sera el leido en i a?diendole la extension .tk, y que contendra una 
     * palabra en cada linea del fichero leido en i.Devolvera true si se 
     * realiza la tokenizacion de forma correcta de todos los archivos que
     contiene i: devolvera false en caso contrario enviando acerr el mensaje
     correspondiente (p.ej. que no exista el archivo i, o bien enviando a
     * "cerr" los archivos de i que no existan, luego no se ha de interrumpir
     * la ejecucion si hay algun archivo en i que no exista)*/
    bool Tokenizador::TokenizarDirectorio (const string& i) const{
        struct stat dir;
        
        int err=stat(i.c_str(), &dir);
        if(err == -1 ||!S_ISDIR(dir.st_mode)){
            return false;
        }else{
            string cmd = "find "+i+" -L -mindepth 1 |sort > .lista_fich";
            system(cmd.c_str());
            return TokenizarListaFicheros(".lista_fich");
        }       
    }
    /* Tokeniza todos los archivos que contenga el directorio i, incluyendo
     los de los subdirectorios, guardando la salida en ficheros cuyo nombre
     sera el de entrada a?diendole la extension .tk, y que contendra una
     palabra en cada linea del fichero. Devolvera true si se realiza la 
     tokenizacion de forma correcta de toos los archivos; devolvera false en
     caso contrario enviando a cerr el mensaje correspondiente (p.ej. que no
     * exista el directorio i, o los ficheros que no se hayan podido tokenizar)*/
    
    void Tokenizador::DelimitadoresPalabra(const string& nuevoDelimiters){
        delimiters = nuevoDelimiters;
        unsigned int longD = delimiters.size();
//        delimitersSet.clear();
//        for (unsigned int i = 0; i < longD; i++){
//                delimitersSet.insert(nuevoDelimiters[i]);
//        }
        specialCaseDelimiters = AddDelimitadoresPalabraToString(" ");
        specialDelimitersWithoutURL = DeleteFromSpecialCaseDelimiters(URL);
        specialDelimitersWithoutEmail = DeleteFromSpecialCaseDelimiters(email);
        specialDelimitersWithoutNumbers = DeleteFromSpecialCaseDelimiters(numbers);
        
    }
    // Cambia delimiters por "nuevoDelimiters"
    
    void Tokenizador::AnyadirDelimitadoresPalabra(const string& nuevoDelimiters){
        unsigned int longNuevoDelimiters = nuevoDelimiters.size();

        for (unsigned int i = 0; i < longNuevoDelimiters; i++){
            
            if (delimiters.find(nuevoDelimiters[i]) == string::npos){
                delimiters += nuevoDelimiters[i];

                //delimitersSet.insert(nuevoDelimiters[i]);
            }  

        }
        specialCaseDelimiters = delimiters + " ";
        specialDelimitersWithoutURL = DeleteFromSpecialCaseDelimiters(URL);
        specialDelimitersWithoutEmail = DeleteFromSpecialCaseDelimiters(email);
        specialDelimitersWithoutNumbers = DeleteFromSpecialCaseDelimiters(numbers);
    }
    /* A?de al final de delimiters los nuevos delimitadores que aparezcan
     * en "nuevoDelimiters" (no se almacenaran caracteres repetidos)
     */
    
    string Tokenizador::AddDelimitadoresPalabraToString(const string& toAddDelimiters){
        string result = delimiters;
        unsigned int longNuevoDelimiters = toAddDelimiters.size();
        for (unsigned int i = 0; i < longNuevoDelimiters; i++){
            
            if (result.find(toAddDelimiters[i]) == string::npos){
                result += toAddDelimiters[i];
            }  
        }
        return result;
    }
    string Tokenizador::DeleteFromSpecialCaseDelimiters(const string& toDeleteDelimiters){
        string result = specialCaseDelimiters;
        for (unsigned int i = 0; i < result.size(); i++){
            
            if (toDeleteDelimiters.find(result[i])!=string::npos){
                result.erase(i,1);
                i --;
            }  
        }
        return result;
    }
//    void Tokenizador::FillDelimitersSet(){
//        for (unsigned int i = 0; i < delimiters.size(); i++){
//            
//            delimitersSet.insert(delimiters[i]);
//        }
//    }

    string Tokenizador::DelimitadoresPalabra() const{
        return this->delimiters;
    }
    //devuelve "Delimiters"
    
    void Tokenizador::CasosEspeciales (const bool& nuevoCasosEspeciales){
        this->casosEspeciales = nuevoCasosEspeciales;
    }
    // Cambian la variable privada "casosEspeciales"

    bool Tokenizador::CasosEspeciales()const{
        return casosEspeciales;
    }
    // Devuelve el contenido de la variable privada "casosEspeciales"
    void Tokenizador::ToLowerWithoutAccent(string& str)const{
        for(unsigned int i = 0; i < str.length(); i ++){

            if(str[i] < 0){
                std::unordered_map<int,int>::const_iterator got = accentToLowerNotAccent.find (str[i]);
                if(got != accentToLowerNotAccent.end()){
                    str[i] = got->second;
                }
            }
            //pasar a minusculas
            else if(str[i] >= 65 && str[i] <=90){
                str[i] += 32;
            }

        } 
    }
    void Tokenizador::ToLowerWithoutAccent1(string& str)const{
        const char* tr = "aaaaaae�eeeeiiiid�ooooox0uuuuypsaaaaaaeceeeeiiiio�ooooo/0uuuuypy";
        for(unsigned int i = 0; i < str.length(); i ++){
            unsigned char ch = str[i];
            if(ch > 191){
                str[i] = tr[ch - 192];
            }
            //pasar a minusculas
            else if(str[i] >= 65 && str[i] <=90){
                str[i] += 32;
            }
        
        }
    }
    
    void Tokenizador::PasarAminuscSinAcentos (const bool& nuevoPasarAminuscSinAcentos){
        pasarAminuscSinAcentos = nuevoPasarAminuscSinAcentos;
    }
    /*
    * Cambia la variable privada pasarAminuscSinAcentos. Atencio al 
    * formato de codificacion del corpus (comando "file" de linux) para la
    * correccion de la practica se utilizara el formato actual ISO-8859
    */

    bool Tokenizador::PasarAminuscSinAcentos()const{
        return pasarAminuscSinAcentos;
    }
    // devuelve el contenido de la variable privada "pasarAminuscSinAcentos"


   