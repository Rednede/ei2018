/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "../include/indexadorInformacion.h"

typedef std::unordered_map<long int, InfTermDoc> tipo_ldocs;

//Fecha
    std::ostream& operator<<(std::ostream& s,const Fecha& p){
        s << p.GetDate();
        return s;
    }
    
    bool operator>(const Fecha& p, const Fecha& p2){
        return p.date > p2.date;
        
    }

    //INFORMACIONTERMINO

    std::ostream& operator<<(std::ostream& s, const InformacionTermino& p) {
        s << "Frecuencia total: " << p.ftc << "\tfd: " << p.l_docs.size();
        //A continuacion se mostrarian todos los elementos de p.l_docs:
        
        for (tipo_ldocs::const_iterator iterator = p.l_docs.begin(); iterator != p.l_docs.end(); ++iterator) {
            s << "\tId.Doc: " << iterator->first << "\t" << iterator->second;
        }
        
        return s;
    }

    InformacionTermino::InformacionTermino (const InformacionTermino& it){
        copia(it);
    }
    InformacionTermino::InformacionTermino (){
        ftc = 0;
    }  //Inicializa ftc = 0
    InformacionTermino::~InformacionTermino (){
        ftc = 0;
        l_docs.clear();
    } //pone ftc = 0 y vacia l_docs
    InformacionTermino & InformacionTermino::operator= (const InformacionTermino & it){
        if(this != &it){
            //(*this).~InformacionTermino();
            InformacionTermino::copia(it);
        }
        return *this;
    }
    
    void InformacionTermino::copia(const InformacionTermino& it){
        ftc = it.ftc;
        l_docs = it.l_docs;
    }
    void InformacionTermino::incrementarLdocs(const long int& idDocPos, const InfTermDoc& ift){
        l_docs.insert({idDocPos,ift });
    }
    
    
    
    //INFTERMDOC
    
    std::ostream& operator<<(std::ostream& s, const InfTermDoc& p){
        s << "ft: " << p.ft;
        
        for (std::list<int>::const_iterator iterator = p.posTerm.begin(), end = p.posTerm.end(); iterator != end; ++iterator) {
                s << "\t" << *iterator;
        }
        
        /*std::for_each(p.posTerm.begin(),p.posTerm.end(), [](const int &element,std::ostream &s){
            
            s << "\t" << element;
        });*/
        return s;
    }
    
    InfTermDoc::InfTermDoc (const InfTermDoc &ift){
        InfTermDoc::copia(ift);
    }
    InfTermDoc::InfTermDoc (){
        ft = 0;

    }  //Inicializa ft = 0
    InfTermDoc::~InfTermDoc (){
        ft = 0;
        posTerm.clear();
    } //Pone ft = 0 
    
    
    InfTermDoc& InfTermDoc::operator= (const InfTermDoc& ift){
        if(this != &ift){
            (*this).~InfTermDoc();
            InfTermDoc::copia(ift);
        }
        return *this;
    }
    void InfTermDoc::copia(const InfTermDoc& ift){
        ft = ift.ft;
        posTerm = ift.posTerm;
    }

    //InfDoc
    std::ostream& operator<<(std::ostream& s, const InfDoc& p){
        s << "idDoc: " << p.idDoc << "\tnumPal: " << p.numPal <<
                "\tnumPalSinParada: " << p.numPalSinParada <<
                "\tnumPalDiferentes: " << p.numPalDiferentes <<
                "\ttamBytes: " << p.tamBytes;
        return s;
    }
    
    InfDoc::InfDoc(const InfDoc& id){
        InfDoc::copia(id);
    }
    InfDoc::InfDoc(){
        
        idDoc = 0;
        numPal = 0;
        numPalDiferentes = 0;
        numPalSinParada = 0;
        tamBytes = 0;
    }
    InfDoc::~InfDoc(){

        idDoc = 0;
        numPal = 0;
        numPalDiferentes = 0;
        numPalSinParada = 0;
        tamBytes = 0;
    }
    InfDoc& InfDoc::operator= (const InfDoc& id){
        if(this != &id){
            (*this).~InfDoc();
            InfDoc::copia(id);
        }
        return *this;
    }
    void InfDoc::copia(const InfDoc& id){
        fechaModificacion = id.fechaModificacion;
        idDoc = id.idDoc;
        numPal = id.numPal;
        numPalDiferentes = id.numPalDiferentes;
        numPalSinParada = id.numPalSinParada;
        tamBytes = id.tamBytes;
    }
    
    //InfColeccionDocs
    
    std::ostream& operator<<(std::ostream& s,const InfColeccionDocs& p){
        s << "numDocs: " << p.numDocs << "\tnumTotalPal: " << p.numTotalPal <<
                "\tnumTotalPalSinParada: " << p.numTotalPalSinParada <<
                "\tnumTotalPalDiferentes: " << p.numTotalPalDiferentes <<
                "\ttamBytes: " << p.tamBytes;
        return s;
    }
    
    InfColeccionDocs::InfColeccionDocs(const InfColeccionDocs& icd){
        InfColeccionDocs::copia(icd);
    }
    InfColeccionDocs::InfColeccionDocs(){
        numDocs = 0;
        numTotalPal = 0;
        numTotalPalDiferentes = 0;
        numTotalPalSinParada = 0;
        tamBytes = 0;
    }
    InfColeccionDocs::~InfColeccionDocs(){
        numDocs = 0;
        numTotalPal = 0;
        numTotalPalDiferentes = 0;
        numTotalPalSinParada = 0;
        tamBytes = 0;
    }
    InfColeccionDocs& InfColeccionDocs::operator= (const InfColeccionDocs& icd){
        if(this != &icd){
            (*this).~InfColeccionDocs();
            InfColeccionDocs::copia(icd);
        }
        return *this;
    }
    void InfColeccionDocs::copia(const InfColeccionDocs& icd){
        numDocs = icd.numDocs;
        numTotalPal = icd.numTotalPal;
        numTotalPalDiferentes = icd.numTotalPalDiferentes;
        numTotalPalSinParada = icd.numTotalPalSinParada;
        tamBytes = icd.tamBytes;
    }
    
    //InformacionTerminoPregunta
    std::ostream& operator<<(std::ostream& s, const InformacionTerminoPregunta& p){
        s << "ft: " << p.ft;
        for (std::list<int>::const_iterator iterator = p.posTerm.begin(), end = p.posTerm.end(); iterator != end; ++iterator) {
                s << "\t" << *iterator;
        }
        return s;
    }
    
    InformacionTerminoPregunta::InformacionTerminoPregunta (const InformacionTerminoPregunta &ift){
        InformacionTerminoPregunta::copia(ift);
    }
    InformacionTerminoPregunta::InformacionTerminoPregunta (){
        ft = 0;
    }  //Inicializa ft = 0
    InformacionTerminoPregunta::~InformacionTerminoPregunta (){
        ft = 0;
        posTerm.clear();
    } //Pone ft = 0 
    InformacionTerminoPregunta& InformacionTerminoPregunta::operator= (const InformacionTerminoPregunta& ift){
        if(this != &ift){
            (*this).~InformacionTerminoPregunta();
            InformacionTerminoPregunta::copia(ift);
        }
        return *this;
    }
    void InformacionTerminoPregunta::copia(const InformacionTerminoPregunta& ift){
        ft = ift.ft;
        posTerm = ift.posTerm;
    }
    
    //InformacionPregunta
    std::ostream& operator<<(std::ostream& s,const InformacionPregunta& p){
        s << "numTotalPal: " << p.numTotalPal <<
                "\tnumTotalPalSinParada: " << p.numTotalPalSinParada <<
                "\tnumTotalPalDiferentes: " << p.numTotalPalDiferentes;
        return s;
    }
    
    InformacionPregunta::InformacionPregunta(const InformacionPregunta& ip){
        InformacionPregunta::copia(ip);
    }
    InformacionPregunta::InformacionPregunta(){
        numTotalPal = 0;
        numTotalPalDiferentes = 0;
        numTotalPalSinParada = 0;
    }
    InformacionPregunta::~InformacionPregunta(){
        numTotalPal = 0;
        numTotalPalDiferentes = 0;
        numTotalPalSinParada = 0;
    }
    InformacionPregunta & InformacionPregunta::operator= (const InformacionPregunta & ip){
        if(this != &ip){
            (*this).~InformacionPregunta();
            InformacionPregunta::copia(ip);
        }
        return *this;
    }
    void InformacionPregunta::copia(const InformacionPregunta& ip){
        numTotalPal = ip.numTotalPal;
        numTotalPalDiferentes = ip.numTotalPalDiferentes;
        numTotalPalSinParada = ip.numTotalPalSinParada;
    }