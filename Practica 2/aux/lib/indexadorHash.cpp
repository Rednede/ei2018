/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   indexadorHash.cpp
 * Author: lourdes
 * 
 * Created on April 19, 2018, 11:12 AM
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include "indexadorHash.h"
typedef std::unordered_map<std::string, InformacionTermino> tipo_indice;
typedef std::unordered_map<std::string, InformacionTerminoPregunta> tipo_indicePregunta;
typedef std::unordered_map<std::string, InfDoc> tipo_indiceDocs;
typedef std::unordered_map<long int, InfTermDoc> tipo_ldocs;


void IndexadorHash::file_to_stopWords(const std::string& filename){
    ifstream i;
        string cadena = "";
        list<string> tokens;
        
        i.open(filename);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << filename << "\n";
            
        }else{
            
            while(getline(i, cadena)){
                
                
                stopWords.insert(cadena);

            }
        }
        i.close();
}
/*"fichStopwords" ser� el nombre del archivo que contendra todas las palabras de parada (una palabra 
     * por cada linea del fichero) y se almacenar� en el campo privado "ficheroStopWords". Asimismo,
     * almacenar� todas las palabras de parada que contenga el archivo en el campo privado "stopWords", 
     * el indice de palabras con parada.
     * "delimitadores" ser� el string que contiene todos los delimitadores utilizados por el tokenizador
     * (campo privado tok).
     * detectComp y minuscSinAcentos seran los parametros que se pasaran al tokenizador.
     * "dirIndice" sera el directorio del disco duro donde se almacenara el indice (campo privado "directorioIndice").
     * Si dirIndice="" entonces se almacenara en el directorio donde se ejecuta el programa.
     * tStemmer inicializara la variable privada "tipoStemmer":
     *      0 = no se aplica stemmer: se indexa el t�rmino tal y como aparece tokenizado
     *      1 = stemmer de porter para espa�ol
     *      2 = stemmer de porter para ingles
     * "almEnDisco" inicializara la variable privada "almacenarEnDisco"
     * "almPosTerm" inicializara la variable privada "almacenarPosTerm"
     * Los indices (p.ej. indice, indiceDocs e informacionColeccionDocs) quedaran vacios
     */
    IndexadorHash::IndexadorHash(const std::string& fichStopWords, const std::string& delimitadores, const bool& detectComp, 
            const bool& minuscSinAcentos, const std::string& dirIndice, const int& tStemmer, 
            const bool& almEnDisco, const bool& almPosTerm){
        ficheroStopWords = fichStopWords;
        file_to_stopWords(ficheroStopWords);
        tok = Tokenizador(delimitadores, detectComp, minuscSinAcentos);
        directorioIndice = dirIndice;
        tipoStemmer = tStemmer;
        almacenarEnDisco = almEnDisco;
        almacenarPosTerm = almPosTerm;
        idDocPos = 1;
        
    }
    
    /*Constructor para incializar IndexadorHash a partir e una indexacion previamente
     * realizada que habra sido almacenada en "directorioIndexacion" mediante el metodo 
     * "bool GuardarIndexacion()". Con ello toda la parte privada se inicializara convenientemente,
     * igual que si se acabase de indexar la coleccion de documentos. En caso que no exista 
     * el directorio o que no contenga los datos de la indexacion se tratara la excepcion correspondiente */
    IndexadorHash::IndexadorHash(const std::string& directorioIndexacion){
        //lee de lo que he guardado en GuardarIndexacion();
    }
    
    IndexadorHash::IndexadorHash(const IndexadorHash& ih){
        IndexadorHash::copia(ih);
    }
    void IndexadorHash::copia(const IndexadorHash& ih){
        almacenarEnDisco = ih.almacenarEnDisco;
        almacenarPosTerm = ih.almacenarPosTerm;
        directorioIndice = ih.directorioIndice;
        ficheroStopWords = ih.ficheroStopWords;
        indice = ih.indice;
        indiceDocs = ih.indiceDocs;
        indicePregunta = ih.indicePregunta;
        infPregunta = ih.infPregunta;
        informacionColeccionDocs = ih.informacionColeccionDocs;
        pregunta = ih.pregunta;
        stopWords = ih.stopWords;
        tipoStemmer = ih.tipoStemmer;
        tok = ih.tok;
        idDocPos = ih.idDocPos;
    }
    
    IndexadorHash::~IndexadorHash(){
        indiceDocs.clear();
        informacionColeccionDocs.~InfColeccionDocs();
        pregunta = "";
        indicePregunta.clear();
        infPregunta.~InformacionPregunta();
        stopWords.clear();
        ficheroStopWords = "";
        tok.~Tokenizador();
        directorioIndice = "";
        idDocPos = 0;
        
    }
    
    IndexadorHash& IndexadorHash::operator= (const IndexadorHash& ih){
        if(this != &ih){
            //(*this).~IndexadorHash();
            IndexadorHash::copia(ih);
        }
        return *this;
    }
    Fecha IndexadorHash::FechaModFichero(const std::string& file)const{
        struct stat stat_buf;
        stat(file.c_str(), &stat_buf);
        
        Fecha f;
        f.SetDate(time(&stat_buf.st_mtime));
        return f;
    }
    
    int IndexadorHash::CalculaTamFichero(const std::string& file)const{
        struct stat stat_buf;
        int rc = stat(file.c_str(), &stat_buf);
        return rc == 0 ? stat_buf.st_size : -1;
    }
    
    /*En el caso que aparezcan documentos repetidos o que ya estuviesen previamente 
     * indexados (ha de coincidir el nombre del documento y el directorio en que 
     * se encuentra),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran 
     * (borrar el documento previamente indexado e indexar el nuevo) en caso que
     * la fecha de modificacion del docuemnto sea mas reciente que la almacenada
     * previamente (class "InfDoc" campo "fechaModificacion").
     * Los caso de reindexacion mantendr�n el mismo idDoc
     */
    bool IndexadorHash::IndexarDocumento(const std::string& file){
   
        ifstream i;
        string cadena;
        
        i.open(file);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << file << "\n";
            return false;
        }else{
            
            //primero buscar si file ya esta, almacenar su peso y cosas
            //se usa mas adelante para actualizar el indiceDocs sin recalcular la pos del file
            tipo_indiceDocs::iterator docu = indiceDocs.find(file);
            if(docu != indiceDocs.end()){
                if (docu->second.GetFechaModificacion() > FechaModFichero(file)){
                    return true;
                }
                else {
                    BorraDoc(file);
                    cerr << "El fichero ya existia y se ha tenido que volver a almacenar" <<"\n";
                    ;
                }
            }
            //vamos a rellenar algunos datos del documento:

            InfDoc id;
            id.SetFechaModificacion(FechaModFichero(file));
            id.SetIdDoc(idDocPos);
            long int tamBytes = CalculaTamFichero(file);
            id.SetTamBytes(tamBytes);
            indiceDocs.insert({file,id});
            docu = indiceDocs.find(file);
            
            //vamos a rellenar algunos datos de informacionColeccionDocs
            informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs() +1);
            informacionColeccionDocs.setTamBytes(informacionColeccionDocs.getTamBytes() + tamBytes);

            
            std::list<string> tokens;
            int pos = 0;
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    //tokenizamos el contenido de cadena
                    tok.Tokenizar(cadena,tokens);
                    //vamos a indexar los tokens
                    
                    for (std::list<std::string>::const_iterator iterator = tokens.begin(), end = tokens.end(); iterator != end; ++iterator) {
                        //para cada palabra
                        //1� miramos si esta en stopwords
                        std::unordered_set<std::string>::const_iterator lwords = stopWords.find(*iterator);
                         
                        if(lwords != stopWords.end()){
                            informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()+ 1);
                            docu->second.SetNumPal(docu->second.GetNumPal() + 1);
                        }
                        else{//si no esta vamos a gestionarla 
                            tipo_indice::iterator word = indice.find(*iterator);
                            if(word == indice.end()){
                                //la palabra no estaba ya almacenada
                                list<int> posTerm;
                                if(almacenarPosTerm){
                                    posTerm.push_back(pos);
                                }
                                //inftermdoc
                                InfTermDoc ifd;
                                ifd.setFt(1);
                                ifd.setPosTerm(posTerm);
                                //ldocs
                                tipo_ldocs l; 

                                l.insert({idDocPos,ifd});
                                //informaciontermino
                                InformacionTermino it;
                                it.setFtc(1);
                                it.setL_docs(l);

                                indice.insert({*iterator,it});
                                
                                //vamos a actualizar indiceDocs                                 
                                docu->second.SetNumPal(docu->second.GetNumPal() + 1);
                                docu->second.SetNumPalSinParada(docu->second.GetNumPalSinParada() + 1);
                                docu->second.SetNumPalDiferentes(docu->second.GetNumPalDiferentes() + 1);
                                
                                
                                //vamos a ocuparnos de infColeccionDocs
                                informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()+ 1);
                                informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() + 1);
                                informacionColeccionDocs.setNumTotalPalDiferentes(informacionColeccionDocs.getNumTotalPalDiferentes() + 1);
                             
                            }                   
                            else{
                                //la palabra ya estaba
                                
                                //vamos a actualizar el indice
                                word->second.setFtc(word->second.getFtc() + 1);
                                //ahora, dependiendo de si esta ya o no en ese fichero
                                InfTermDoc ift;
                                if(!word->second.devuelveInfTermDoc(idDocPos, ift)){
                                    //no estaba
                                    
                                    //inftermdoc
                                    if(almacenarPosTerm){
                                        ift.insertar(pos);
                                    }else {
                                        ift.setFt(ift.getFt() + 1);
                                    }
                                    
                                    
                                    //lo metemos en el informaciontermino
                                    word->second.incrementarLdocs(idDocPos,ift);
                                    
                                    docu->second.SetNumPalDiferentes(docu->second.GetNumPalDiferentes() + 1);
                                        
                                }else{//ya estaba en el doc
                                    if(almacenarPosTerm){
                                        ift.insertar(pos);
                                    }else {
                                        ift.setFt(ift.getFt() + 1);
                                    }
                                    word->second.actualizaInfTermDoc(idDocPos, ift);
                                    //tipo_ldocs::iterator ld = word->second.getL_docs().find(idDocPos);
                                    //ld;
                                }
                                
                                
                                
                                //vamos a actualizar indiceDocs
                                docu->second.SetNumPal(docu->second.GetNumPal() + 1);
                                docu->second.SetNumPalSinParada(docu->second.GetNumPalSinParada() + 1);
                                
                                //vamos a ocuparnos de infColeccionDocs
                                informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()+ 1);
                                informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() + 1);
                            }
                        } 
                        ++pos;
                    }
                }
            }
            //lo preparamos para que el siguiente doc que se lea tenga otro id
            ++ idDocPos ;
        }
        i.close();
        return true;
    
    }
    
    /*Devuelve true si consigue crear el indice para la coleccion de docuemntos
     * detallada en ficheroDocumentos, el cual contendra un monbre de documento
     * por linea. Los a�adira a los ya existentes anteriormente en el indice
     * Devuelve falso sino  finaliza la indexacion p.ej por falta de memoria, 
     * mostrando el mensaje de excepcion correspondiente, indicando el documento 
     * y termino en el que se ha quedado
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente 
     * indexados (ha de coincidir el nombre del documento y el directorio en que 
     * se encuentra),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran 
     * (borrar el documento previamente indexado e indexar el nuevo) en caso que
     * la fecha de modificacion del docuemnto sea mas reciente que la almacenada
     * previamente (class "InfDoc" campo "fechaModificacion").
     * Los caso de reindexacion mantendr�n el mismo idDoc
     */
    bool IndexadorHash::Indexar(const std::string& ficheroDocumentos){
        
        ifstream i;
        string cadena;
        
        i.open(ficheroDocumentos);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << ficheroDocumentos << "\n";
            return false;
        }else{
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    //indexamos el contenido de cadena
                    IndexarDocumento(cadena);
                }
            }
        }
        i.close();
        
        
    }
    
    /* Devuelve true si consigue crear el indice para la coleccion de documentos
     * que se encuentra en el directorio (y subdirectorios que contenga) dirAIndexar 
     * (independientemente de extension de los mismos). Se considerara que todos los
     * documentos del directorio seran ficheros de texto. Los a�adir� a los ya existentes
     * anteriormente en el indice.
     * Devuelve falso si no finaliza la indexacion p.ej por falta de memoria o porque no
     * exista dirAIndexar, mostrando el mensaje de error correspondiente, indicando el documento y 
     * t�rmino en el que se ha quedado.
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente
     * indexados (ha de coincidir el nombre del documento y el directorio en el que se encuentre),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran (borrar el documento
     * previamente indexado e indexar de nuevo) en caso que la fecha de modificacion del documento 
     * sea mas reciente que la almacenada previamente (class "InfDoc" campo "fechaModificacion").
     * Los casos de reindexacion mantendran el mismo idDoc
     */
    bool IndexadorHash::IndexarDirectorio(const std::string& dirAIndexar){
        DIR *dir;
        struct dirent *ent;
        const char * direccion = dirAIndexar.c_str();
        if ((dir = opendir (direccion)) != NULL) {
         while ((ent = readdir (dir)) != NULL) {
             string s = string(ent->d_name);
             if (s.substr(0,1) != "."){
                switch (ent->d_type) {
                    case DT_REG:
                        // Regular file
                        IndexarDocumento(dirAIndexar + "/" +s);
                        break;
                    case DT_DIR:
                        // Directory
                        IndexarDirectorio(dirAIndexar + "/  " +s);
                        
                        break;
                    default:
                        return false;
                }
            }
        }
         closedir (dir);
        } else {
            cerr << "ERROR: No se ha podido abrir la carpeta: " << dirAIndexar << endl;
            return false;
        }  
        return true;
    }
    
    /* Se guardara en disco duro (directorio contenido en la variable privada "directorioIndice")
     * la indexacion actualmente en memoria (incluidos todos los parametros de la parte privada).
     * La forma de almacenamiento la determinara el alumno. El objetivo es que esta indexacion
     * se pueda recuperar posteriormente mediante el constructor "IndexadorHash(const string&)".
     * Por ejemplo, supongamos que se ejecuta esta secuencia de comandos: 
     * "IndexadorHash a ("./fichStopWords.txt", "[ ,.", "./dirIndexPrueba", 0, false);
     * "a.Indexar("./fichConDocsAIndexar.txt"); a.GuardarIndexacion();"
     * Entonces mediante el comando: "IndexadorHash b("./dirIndexPrueba");" se recuperara la 
     * indexacion realizada en la secuencia anterior, cargandola en b.
     * Devuelve falso si no finaliza la operacion (p.ej por falta de moemoria o el nombre del directorio
     * contenido en directorioIndice no es correcto), mostrando el mensaje de error correspondiente.
     * en caso de que no existiese el directorio directorioIndice, habr�a que crearlo previamente
     * 
     */
    bool IndexadorHash::GuardarIndexacion() const{
        DIR *dir;
        struct dirent *ent;
        const char * direccion = directorioIndice.c_str();
        if ((dir = opendir (direccion)) == NULL) {
            string d = "mkdir -p ";
            d += directorioIndice;
            const int dir_err = system(d.c_str());
            if (-1 == dir_err)
            {
                cerr << "Error creando el directorio: " << directorioIndice;
                return false;
            }
        }
        //ahora ya hay directorio, ya se puede guardar

        ofstream i;
        //empezamos guardando la pregunta 
        i.open(directorioIndice + "/InformacionPregunta");
        if(!i){
            //si no existe se crea
            i.open(directorioIndice + "/InformacionPregunta",  fstream::out | fstream::trunc);

        }
        //guardamos la indexacion de la pregunta
            i << pregunta << "\n";
            i << infPregunta << "\n";
            for (tipo_indicePregunta::const_iterator iterator = indicePregunta.begin(); iterator != indicePregunta.end(); ++iterator) {
                i << iterator->first << "\n" << iterator->second << "\n";
            } 

        i.close();
        //procedemos a gestionar los documentos
        i.open(directorioIndice + "/InfDoc");
        if(!i){
            //si no existe se crea
            i.open(directorioIndice + "/InfDoc",  fstream::out | fstream::trunc);

        }
        //guardamos Informacion coleccion Docs
        i << informacionColeccionDocs<< "\n";
        //guardamos IndiceDocs
        for (tipo_indiceDocs::const_iterator iterator = indiceDocs.begin(); iterator != indiceDocs.end(); ++iterator) {
            i<< iterator->first << '\t' << iterator->second << "\t"<<  iterator->second.GetFechaModificacion() <<"\n";
        }
        i.close();
        
        //Ahora vamos a guardar el indice
        i.open(directorioIndice + "/Indice");
        if(!i){
            //si no existe se crea
            i.open(directorioIndice + "/Indice",  fstream::out | fstream::trunc);

        }

        //guardamos IndiceDocs
        for (tipo_indice::const_iterator iterator = indice.begin(); iterator != indice.end(); ++iterator) {
            i<< iterator->first << '\n' << iterator->second << "\n";
        }
        i.close();

        //finalmente guardamos varias variables
        i.open(directorioIndice + "/Varios");
        if(!i){
            //si no existe se crea
            i.open(directorioIndice + "/Varios",  fstream::out | fstream::trunc);

        }
        
        

        i << tipoStemmer <<"\n";
        i << almacenarEnDisco <<"\n";
        i << almacenarPosTerm <<"\n";
        i << tok.CasosEspeciales() << "\n";
        i << tok.PasarAminuscSinAcentos() <<"\n";
        i << tok.DelimitadoresPalabra() << "\n";
        i << ficheroStopWords <<"\n";
        for (std::unordered_set<string>::const_iterator iterator = stopWords.begin(), end = stopWords.end(); iterator != end; ++iterator) {
                i  << *iterator<< "\n";
        }
       
        i.close();
        
    }
    
    /* Vacia la indexacion que tuviese en ese momento e inicializa IndexadorHash a partir
     * de una indexacion previamente realizada que habra sido almacenada en "directorioIndexacion"
     * mediante el metoo "bool GuardarIndexaxion();". Con ello toda la parte privada se inicializara
     * ctonvenientemente, igual que si se acabase de indexar la coleccion de documentos.
     * En caso que no existiera el directorio o que no contengalos datos de la indexaci�n
     * se tratar� la excepcion correspondiente, y se devolvera false
     */
    bool IndexadorHash::RecuperarIndexacion(const std::string& directorioIndexacion){
        indice.clear();
        indiceDocs.clear();
        stopWords.clear();
        indicePregunta.clear();
        

        
        ifstream i;
        string cadena = "";
        Tokenizador t("\t ", false, false);
        list<string> tokens;
        InfDoc id;
        InfTermDoc itdclass;
        InformacionTermino itclass;
        string nombre;
        Fecha f;
        //varios
        i.open(directorioIndice + "/Varios");
        if(!i){
            cerr << "no se ha encontrado " << directorioIndice << "/Varios";
            return false;
        }
        getline(i, cadena);
        tipoStemmer = std::stoi(cadena);
        getline(i, cadena);
        almacenarEnDisco = std::stoi(cadena);
        getline(i, cadena);
        almacenarPosTerm = std::stoi(cadena);
        getline(i, cadena);
        tok.CasosEspeciales(std::stoi(cadena));
        getline(i, cadena);
        tok.PasarAminuscSinAcentos(std::stoi(cadena));
        getline(i,cadena);
        tok.setDelimiters(cadena);
        getline(i, cadena);
        ficheroStopWords = cadena;
        while(getline(i, cadena)){
            stopWords.insert(cadena);
        }
        i.close();
        //IndiceDocs
        i.open(directorioIndice + "/InfDoc");
        if(!i){
            cerr << "no se ha encontrado " << directorioIndice << "/InfDoc";
            return false;
        }
        
        //recuperamos Informacion coleccion Docs
        getline(i, cadena);
        t.Tokenizar(cadena, tokens);
        list<string>::const_iterator it = tokens.begin();
        ++it;
        informacionColeccionDocs.setNumDocs(std::stol(*it));
        ++it;++it;
        informacionColeccionDocs.setNumTotalPal(std::stol(*it));
        ++it;++it;
        informacionColeccionDocs.setNumTotalPalSinParada(std::stol(*it));
        ++it;++it;
        informacionColeccionDocs.setNumTotalPalDiferentes(std::stol(*it));
        ++it;++it;
        informacionColeccionDocs.setTamBytes(std::stol(*it));
        //guardamos IndiceDocs
        while(getline(i, cadena)){
            t.Tokenizar(cadena, tokens);
            list<string>::const_iterator it = tokens.begin();
            nombre = *it;
            ++it, ++it;
            id.SetIdDoc(stol(*it));
            ++it; ++it;
            id.SetNumPal(stoi(*it));
            ++it; ++it;
            id.SetNumPalSinParada(stoi(*it));
            ++it; ++it;
            id.SetNumPalDiferentes(stoi(*it));
            ++it; ++it;
            id.SetTamBytes(stoi(*it));
            ++it;
            f.SetDate(stol(*it));
            id.SetFechaModificacion(f);
            indiceDocs.insert({nombre, id});

        }

        i.close();
        
        //indice

        i.open(directorioIndice + "/Indice");
        if(!i){
            cerr << "no se ha encontrado " << directorioIndice << "/Indice";
            return false;
        }
        while(getline(i, cadena)){
            nombre = cadena;
            getline(i, cadena);
            t.Tokenizar(cadena, tokens);
            list<string>::const_iterator it = tokens.begin();
            ++it;++it;
            itclass.setFtc(std::stoi(*it));
            
            ++it;++it;
            int numDocs = std::stoi(*it);
            for (int i = 0; i < numDocs;i++){
                long int idDoc;
                ++it; ++it;
                idDoc = std::stol(*it);
                
                ++it; ++it;
                int ft = std::stoi(*it);
                itdclass.setFt(ft);
                std::list<int> listapt;
                if(almacenarPosTerm) {
                    for(int k = 0; k < ft; ++k) {
                        ++it;
                        listapt.push_back(std::stoi(*it));
                    }
                }
                
                itdclass.setPosTerm(listapt);
                itclass.incrementarLdocs(idDoc, itdclass);
            }
            
            indice.insert({nombre, itclass});
        }
        
        //la pregunta
        i.open(directorioIndice + "/InformacionPregunta");
        if(!i){
            cerr << "no se ha encontrado " << directorioIndice << "/InformacionPregunta";
            return false;
        }
        
        getline(i, cadena);
        if(cadena.length()!= 0){
            //entonces hay pregunta
            pregunta = cadena;
            cadena = "";
            
            //recogemos la informacion
            getline(i, cadena);
            t.Tokenizar(cadena, tokens);
            list<string>::const_iterator it = tokens.begin();
            it++;
            infPregunta.setNumTotalPal(std::stol(*it));
            it++;it++;
            infPregunta.setNumTotalPalSinParada(std::stol(*it));
            it++; it++;
            infPregunta.setNumTotalPalDiferentes(std::stol(*it));
            //recogemos los terminos indexados
            while(getline(i, cadena)){
                nombre = cadena;
                getline(i, cadena);
                t.Tokenizar(cadena, tokens);
                list<string>::const_iterator it = tokens.begin();
                
                it++; 
                int ft = std::stoi(*it);
                list<int> pt;
                if (almacenarPosTerm){
                    for (int i = 0; i < ft; ++ i){
                        it++;
                        pt.push_back(std::stoi(*it));
                    }
                }
                InformacionTerminoPregunta itp;
                itp.setFt(ft);
                itp.setPosTerm(pt);
                indicePregunta.insert({nombre, itp});
            }

        }
        
    }
    
    
    void IndexadorHash::ImprimirIndexacion()const{
        cout << "Terminos indexados: \n" ;
        /*listado del contenido del campo privado indice donde para cada termino indedaxo 
        se imprimira cout << termino << "\t" << InformacionTermino << "\n";*/
        for(tipo_indice::const_iterator ind = indice.begin(); ind != indice.end(); ind ++){
            cout << ind->first << "\t" << ind->second << "\n";
        }
        
        cout<< "Documentos indexados: \n";
        /*a continuacion aparecera un listado del contenido del campo privado "indiceDocs" donde para 
        cada documento indexado se imprimira cout<< nomDoc << "\t" << InfDoc << "\n";*/
        for(tipo_indiceDocs::const_iterator indD = indiceDocs.begin(); indD != indiceDocs.end(); indD ++){
            cout << indD->first << "\t" << indD->second << "\n";
        }
    }
    
    
    
    /*Devuelve true si consigue crear el indice para la pregunta "preg".
     * antes de realizar la indexacion vaciara los campos privados indicePregunta
     * e infPregunta. Generara la misma informacion que en la indexacion de documentos
     * pero dejandola toda accesible en memoria principal (mediante las variables privadas
     * "pregunta, indicePregunta, infPregunta")
     * Devuelve false si no finaliza la operacion (p.ej por falta de memoria o bien 
     * si la pregunta no contiene ningun termino con contenido), comstrando el mensaje
     * de error correspondiente
     */
    bool IndexadorHash::IndexarPregunta(const std::string& preg){
        bool aDevolver = false;
        indicePregunta.clear();
        infPregunta.~InformacionPregunta();
        std::list<std::string> lPreg;
        tok.Tokenizar(preg,lPreg);
        InformacionTerminoPregunta itp;
        int pos = 0;
        infPregunta.setNumTotalPal(lPreg.size());
        
        for(std::list<std::string>::iterator iterator = lPreg.begin(); iterator != lPreg.end();){

            std::unordered_set<std::string>::const_iterator lwords = stopWords.find(*iterator);
            if(lwords != stopWords.end()){
                iterator = lPreg.erase(iterator);
            }
            //else no es un stopwords vamos a almacenarla en indicepregunta
            else{
                aDevolver = true;
                pregunta = preg;
                tipo_indicePregunta::iterator ipreg = indicePregunta.find(*iterator);
                //si ya estaba la palabra una vez
                if(ipreg != indicePregunta.end()){
    
                    list<int> lpos = ipreg->second.getPosTerm();
                    lpos.push_back(pos);
                            
                     ipreg->second.setFt(ipreg->second.getFt() + 1);
                     ipreg->second.setPosTerm(lpos);
                     infPregunta.setNumTotalPalSinParada(infPregunta.getNumTotalPalSinParada() + 1); 
                    
                }else{
                    
                    itp.setFt(1);
                    list<int> lpos;
                    lpos.push_back(pos);
                    itp.setPosTerm(lpos);//meter la posicion del termino
                    indicePregunta.insert({{*iterator},{itp}});
                    infPregunta.setNumTotalPalSinParada(infPregunta.getNumTotalPalSinParada() + 1);
                    infPregunta.setNumTotalPalDiferentes(infPregunta.getNumTotalPalDiferentes() + 1);
                    
                }
                 ++ iterator;
            }
            ++pos;
        }    
        return aDevolver;
        
    }
    
    /*Devuelve true si hay una pregunta indexada (con al menos un termino que no sea
     * palabra de parada, o sea que haya algun termino indexado en indicePregunta), 
     * devolviendola en "preg"
     */
    bool IndexadorHash::DevuelvePregunta(std::string& preg) const{
        preg = pregunta;
        return (preg != "");
    }
    
    /*Devuelve true si word esta indexado en la pregunta, devolviendo su informacion
     * almacenada "inf". En caso que no este devolveria devolveria "inf" vacio*/
    bool IndexadorHash::DevuelvePregunta(const std::string& word, InformacionTerminoPregunta& inf)const{
        //cambiar por find
        tipo_indicePregunta::const_iterator iterator = indicePregunta.find(word);
        if(iterator != indicePregunta.end()){
            inf = iterator->second;
                    return true;
        }
        return false;        
    }
    
    /*Devuelve true si hay una pregunta indexada, devolviendo su informacion almacenada
     * (campo privado "infPregunta")en "inf". En caso que no este, devolveria "inf" vacio
     */
    bool IndexadorHash::DevuelvePregunta(InformacionPregunta& inf)const{
        if (!indicePregunta.empty()){
            inf = infPregunta;
            return true;
        }
        return false;
    }
    
    void IndexadorHash::ImprimirIndexacionPregunta()const{
        cout << "Pregunta indexada: " << pregunta << "\n";
        cout << "Terminos indexados en la pregunta: " << "\n";
        for (tipo_indicePregunta::const_iterator iterator = indicePregunta.begin(); iterator != indicePregunta.end(); ++iterator) {
                cout << iterator->first << "\t" << iterator->second << "\n";
            } 

        cout << "Informacion de la pregunta: " << infPregunta << "\n";
    }
    

    
    void IndexadorHash::ImprimirPregunta()const{
        cout << "Pregunta indexada: " << pregunta << "\n";
        cout << "Informacion de la pregunta: " << infPregunta << "\n";
    }
    
    /*Devuelve true si wor esta indexado, devolviendo su informacion almacenada "inf".
     * En caso que no este, devolveria "inf" vacio
     */
    bool IndexadorHash::Devuelve(const std::string& word, InformacionTermino& inf)const{
        tipo_indice::const_iterator iterator = indice.find(word);
        if (iterator != indice.end()){ 
            inf = iterator->second;
            return true;
        }
            inf = InformacionTermino();

        return false;
         
    }
    
    /*Devuelve true si word esta indexado y aparece en el documento de nobre nomDoc, 
     * en cuyo caso devuelve la inormacion alamacenada para word en el documento.
     * En caso que noeste, devolveria "InfDoc" vacio
     */
    bool IndexadorHash::Devuelve(const std::string& word, const std::string& nomDoc, InfTermDoc& InfDoc)const{
        
        tipo_indice::const_iterator iterator = indice.find(word); 
        if (iterator != indice.end()){
            tipo_indiceDocs::const_iterator iterator2 = indiceDocs.find(nomDoc);
            if(iterator2 != indiceDocs.end()){
                //el id
                //iterator2->second.GetIdDoc();
                iterator->second.devuelveInfTermDoc(iterator2->second.GetIdDoc(), InfDoc);
                /*tipo_ldocs::const_iterator iterator3 = iterator->second.getL_docs().find(iterator2->second.GetIdDoc());
                if(iterator3 != iterator->second.getL_docs().end()){
                    InfDoc = iterator3->second;*/
                    return true;

            }
        }
        
        return false;
    }
    
    //Devuelve true si word aparece como termino indexado
    bool IndexadorHash::Existe(const std::string& word)const{
        tipo_indice::const_iterator iterator = indice.find(word); 
        return (iterator != indice.end());
    }
    
    //Devuelve true si se realiza el borrado p.ej si word aparece como termino indexado
    bool IndexadorHash::Borra(const std::string& word){
        
        if (indice.erase(word) == 1){
            return true;
        }
        return false;
    }
    
    /*Devuelve true si nomDoc esta indexado y se realiza el borrado de todos los 
     * terminos del documento y del docuemnto en los campos privados "indiceDocs" e
     * "informacionColeccionDocs"
     */
    bool IndexadorHash::BorraDoc(const std::string& nomDoc){
        
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc);
        if(iterator !=indiceDocs.end()){
            long int id = iterator->second.GetIdDoc();

            if(iterator != indiceDocs.end()){
                informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs() - 1);
                informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()- iterator->second.GetNumPal() );
                informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() - iterator->second.GetNumPalSinParada() );
                //informacionColeccionDocs.setNumTotalPalDiferentes(informacionColeccionDocs.getNumTotalPalDiferentes() - iterator->second.GetNumPalDiferentes() );
                informacionColeccionDocs.setTamBytes(informacionColeccionDocs.getTamBytes() - iterator->second.GetTamBytes());

                for (tipo_indice::iterator iteratorind = indice.begin(), end = indice.end(); iteratorind != end; ) {
                    InfTermDoc itd;
                    if (iteratorind->second.devuelveInfTermDoc(id, itd)){
                        iteratorind->second.setFtc(iteratorind->second.getFtc() - itd.getFt());
                        iteratorind->second.borraPorIdDoc(id);
                        if(iteratorind->second.getL_docs().empty()){
                            //eliminamos el elemento del indice
                            iteratorind = indice.erase(iteratorind);
                            informacionColeccionDocs.setNumTotalPalDiferentes(informacionColeccionDocs.getNumTotalPalDiferentes() - 1);
                        }
                        else{++iteratorind;}
                    }else{++iteratorind;}
                }

                indiceDocs.erase(iterator);
                return true;

            }
        }
        return false;
    }
    
    //Borra todos los terminos del indice de documento
    void IndexadorHash::VaciarIndiceDocs(){
        indiceDocs.clear();
    }
    
    //Borra todos los terminos del indice de la pregunta
    void IndexadorHash::VaciarIndicePreg(){
        indicePregunta.clear();
    }
    
    //sera true si word esta indexado, sustituyendo la informacion almacenada por "inf"
    bool IndexadorHash::Actualiza(const std::string& word, const InformacionTermino& inf){
        tipo_indice::iterator iterator = indice.find(word); 
        if (iterator != indice.end()){
            iterator->second = inf;

            return true;
        }
        return false;
    }
    
    //Sera true si se realiza la insercion p.ej si word no estaba previamente indexado
    bool IndexadorHash::Inserta(const std::string& word, const InformacionTermino& inf){
        tipo_indice::const_iterator iterator = indice.find(word); 
        if (iterator == indice.end()){
            indice.insert({{word}, {inf}});
            

            return true;
        }
        return false;
    }
    
    //Devolvera el numero de terminos diferentes indexados (cardinalidad de campo privado "indice")
    int IndexadorHash::NumPalIndexadas()const{
        return indice.size();
    }
    
    //Devuelve el contenido del campo privado "ficheroStopWords"
    std::string IndexadorHash::DevolverFichPalParada()const{
        return ficheroStopWords;
    }
    
    /*Mostrara por pantalla las paralbras de paraa almacenadas (originales, sin aplicar stemming):
     * una plabra por linea (salto de linea al final de cada palabra)
     */
    void IndexadorHash::ListarPalParada()const{
        for (std::unordered_set<string>::const_iterator iterator = stopWords.begin(), end = stopWords.end(); iterator != end; ++iterator) {
                cout  << *iterator<< "\n";
        }
    }
    
    //Devolvera el numero de palabras de parada almacenadas
    int IndexadorHash::NumPalParada() const{
        return stopWords.size();
    }
    
    //Devuelve los delimitadores utilizados por el tokenizador
    std::string IndexadorHash::DevolverDelimitadores() const{
        return tok.getDelimiters();
    }
    
    //Devuelve si el tokenizador analiza los casos especiales
    bool IndexadorHash::DevolverCasosEspeciales () const{
        return tok.isCasosEspeciales();
    }
    
    //Devuelve si el tokenizador pasa a minusculas y sin acentos
    bool IndexadorHash::DevolverPasarAminuscSinAcentos () const{
        return tok.isPasarAminuscSinAcentos();
    }
    
    //Devuelve el valor de almacenarPosTerm
    bool IndexadorHash::DevolverAlmacenarPosTerm() const{
        return almacenarPosTerm;
    }
    
    //Devuelve "directorioIndice" (el directorio del disco duro donde se almacenara el indice)
    std::string IndexadorHash::DevolverDirIndice() const{
        return directorioIndice;
    }
    
    //Devolvera el tipo de stemming realizado en la indexcion de acuerdo con el valo de "sipoStemmer"
    int IndexadorHash::DevolverTipoStemming() const{
        return tipoStemmer;
    }
    
    //Devolvera el valor indicado en la variable privada "almEnDisco"
    bool IndexadorHash::DevolverAlmEnDisco() const{
        return almacenarEnDisco;
    }
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indice": */
    void IndexadorHash::ListarInfColeccDocs() const{
        cout << informacionColeccionDocs<< "\n";
    }
    
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla todos 
     * los terminos indexados del documento con nombre 
     * "nomDoc": cout << termino << '\t' << InformacionTermino << endl;
     * si no existe no se muestra nada*/
    bool IndexadorHash::ListarTerminos(const std::string& nomDoc)const{
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc); 
        if (iterator != indiceDocs.end()){
            for (tipo_indice::const_iterator terminosind = indice.begin(); terminosind != indice.end(); ++terminosind) {
                tipo_ldocs::const_iterator iterator2 = terminosind->second.getL_docs().find(iterator->second.GetIdDoc());
                if(iterator2 != terminosind->second.getL_docs().end() ){
                    cout << terminosind->first << '\t' << terminosind->second << "\n";
                }
            }
            return true;
         }
        return false;
    }
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indiceDocs": cout<< nomDoc << '\t' << infDoc << endl;
     */
    void IndexadorHash::ListarDocs() const{
        for (tipo_indiceDocs::const_iterator iterator = indiceDocs.begin(); iterator != indiceDocs.end(); ++iterator) {
            cout<< iterator->first << '\t' << iterator->second << "\n";
        }
    }
   
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla el contenido
     * del campo privado indiceDocs para el docuemnto con nombre 
     * "nomDoc": cout << nomDoc << '\t' << infDoc << endl;
     * si no existe no se muestra nada*/
    bool IndexadorHash::ListarDocs(const std::string& nomDoc) const{
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc);
        if(iterator != indiceDocs.end()){
            cout << nomDoc << '\t' << iterator->second << "\n";
            return true;
        }
        return false;
    
    }