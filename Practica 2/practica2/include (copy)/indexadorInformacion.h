/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   indexadorInformacion.h
 * Author: lourdes
 *
 * Created on April 19, 2018, 11:20 AM
 */

#ifndef INDEXADORINFORMACION_H
#define INDEXADORINFORMACION_H
#include <iostream>
#include <unordered_map>
#include <list>
#include <chrono>
#include <ctime>
#include <time.h>
#include <algorithm>


class Fecha {
    friend std::ostream& operator<<(std::ostream& s, const Fecha& p);
private:
    std::time_t date;
public:
    Fecha() {
        //date = std::system_clock::to_time_t(std::system_clock::now());
        time(&date);
    }
    ~Fecha(){
    
    }
    std::time_t GetDate() const {
        return date;
    }

    void SetDate(std::time_t date) {
        this->date = date;
    }
    Fecha(const Fecha& other) :
    date(other.date) {
    } 
    void copia(const Fecha & ip){
        date = ip.date;
    }
    Fecha & operator= (const Fecha & ip){
        if(this != &ip){
            (*this).~Fecha();
            copia(ip);
        }
        return *this;
    }


};



class InfTermDoc {
    
    
    friend std::ostream& operator<<(std::ostream& s, const InfTermDoc& p);
public:
    InfTermDoc (const InfTermDoc &);
    InfTermDoc ();  //Inicializa ft = 0
    ~InfTermDoc (); //Pone ft = 0 
    InfTermDoc& operator= (const InfTermDoc&);
    void copia(const InfTermDoc&);
        //getters y setters
    int getFt() const {
        return ft;
    }

    void setFt(int ft) {
        this->ft = ft;
    }

    std::list<int> getPosTerm() const {
        return posTerm;
    }

    void setPosTerm(std::list<int> posTerm) {
        this->posTerm = posTerm;
    }
   



private:
    int ft;
    std::list<int> posTerm;

    
};

class InformacionTermino{
    friend std::ostream& operator<<(std::ostream& s, const InformacionTermino& p);

public:
    InformacionTermino (const InformacionTermino&);
    InformacionTermino ();  //Inicializa ftc = 0
    ~InformacionTermino (); //pone ftc = 0 y vacia l_docs
    InformacionTermino & operator= (const InformacionTermino &);
    void copia(const InformacionTermino&);

    //getters y setters
    int getFtc() const {
        return ftc;
    }

    void setFtc(int ftc) {
        this->ftc = ftc;
    }

    std::unordered_map<long int, InfTermDoc> getL_docs() const {
        return l_docs;
    }

    void setL_docs(std::unordered_map<long int, InfTermDoc> l_docs) {
        this->l_docs = l_docs;
    }

    
private:
    int ftc;    //frecuencia total del termino en la coleccion
    std::unordered_map<long int, InfTermDoc> l_docs;
        /*tabla hash que se accedera por el id del documento, devolviendo un objeto
         * de la clase InfTermDoc que contiene toa la informaci�n de aparici�n del 
         * termino en el documento*/
};

class InfDoc{
    friend std::ostream& operator<<(std::ostream& s, const InfDoc& p);
public:
    InfDoc(const InfDoc&);
    InfDoc();
    ~InfDoc();
    InfDoc& operator= (const InfDoc&);
    void copia(const InfDoc&);
    
    
    Fecha GetFechaModificacion() const {
        return fechaModificacion;
    }

    void SetFechaModificacion(Fecha fechaModificacion) {
        this->fechaModificacion = fechaModificacion;
    }

    long int GetIdDoc() const {
        return idDoc;
    }

    void SetIdDoc(long int idDoc) {
        this->idDoc = idDoc;
    }

    int GetNumPal() const {
        return numPal;
    }

    void SetNumPal(int numPal) {
        this->numPal = numPal;
    }

    int GetNumPalDiferentes() const {
        return numPalDiferentes;
    }

    void SetNumPalDiferentes(int numPalDiferentes) {
        this->numPalDiferentes = numPalDiferentes;
    }

    int GetNumPalSinParada() const {
        return numPalSinParada;
    }

    void SetNumPalSinParada(int numPalSinParada) {
        this->numPalSinParada = numPalSinParada;
    }

    int GetTamBytes() const {
        return tamBytes;
    }

    void SetTamBytes(int tamBytes) {
        this->tamBytes = tamBytes;
    }

    
private:
    long int idDoc;
    /*Identificador del documento. El primerdocumento indexado en la  coleccion sera
     * el identificador 1
     */
    int numPal;             //n� total de palabras del documento
    int numPalSinParada;    //n� total de palabas sin stop-word del documento
    int numPalDiferentes;   
    /*n� total de palabras diferentes que no sean stop-word 
     * (sin acumular la frecuencia de cada una de ellas)
     */
    int tamBytes;           //tama�o en bytes del documento
    Fecha fechaModificacion;
        /*Atributo correspondiente a la fecha y hora de modificacion del documento.
         * el tipo Fecha/Hora lo elegira/ implementar� el alumno*/
};
 
class InfColeccionDocs{
    friend std::ostream& operator<<(std::ostream& s, const InfColeccionDocs & p);
public:
    InfColeccionDocs (const InfColeccionDocs &);
    InfColeccionDocs ();
    ~InfColeccionDocs ();
    InfColeccionDocs & operator= (const InfColeccionDocs &); 
    void copia (const InfColeccionDocs &);
    
    long int getNumDocs() const {
        return numDocs;
    }

    void setNumDocs(long int numDocs) {
        this->numDocs = numDocs;
    }

    long int getNumTotalPal() const {
        return numTotalPal;
    }

    void setNumTotalPal(long int numTotalPal) {
        this->numTotalPal = numTotalPal;
    }

    long int getNumTotalPalDiferentes() const {
        return numTotalPalDiferentes;
    }

    void setNumTotalPalDiferentes(long int numTotalPalDiferentes) {
        this->numTotalPalDiferentes = numTotalPalDiferentes;
    }

    long int getNumTotalPalSinParada() const {
        return numTotalPalSinParada;
    }

    void setNumTotalPalSinParada(long int numTotalPalSinParada) {
        this->numTotalPalSinParada = numTotalPalSinParada;
    }

    long int getTamBytes() const {
        return tamBytes;
    }

    void setTamBytes(long int tamBytes) {
        this->tamBytes = tamBytes;
    }

    
private:
    long int numDocs;       //N� total de documentos en la coleccion
    long int numTotalPal;   //n� total de palabras en la coleccion
    long int numTotalPalSinParada;
    long int numTotalPalDiferentes; 
    /* n� total de palabras diferentes en la coleccion
     * que no sean stop words, sin acumular la frecuencia*/
    long int tamBytes;      //tama�o total de bytes en la coleccion
};

class InformacionTerminoPregunta {
    friend std::ostream& operator<<(std::ostream& s, const InformacionTerminoPregunta& p);
public:
    InformacionTerminoPregunta (const InformacionTerminoPregunta &);
    InformacionTerminoPregunta ();
    ~InformacionTerminoPregunta ();
    InformacionTerminoPregunta & operator= (const InformacionTerminoPregunta &); 
    void copia (const InformacionTerminoPregunta &);
    int getFt() const {
        return ft;
    }

    void setFt(int ft) {
        this->ft = ft;
    }

    std::list<int> getPosTerm() const {
        return posTerm;
    }

    void setPosTerm(std::list<int> posTerm) {
        this->posTerm = posTerm;
    }

    
private:
    int ft;     //Frecuencia total del t�rmino en la pregunta
    std::list<int> posTerm;
        /* Solo se almacenara esta informacion si el campo privado del indexador
         * almacenrPosTerm == true. lista de numeros de palabraen los que aparece 
         * el termino en la pregunta. Los n�meros de palabracomenaran desde cero 
         * (la primera palabra de la pregunta). Se numeraran las palabras de parada.
         * Estar� ordenada de menor a mayor posicion.
         */
    
};

class InformacionPregunta {
    friend std::ostream& operator<<(std::ostream& s, const InformacionPregunta& p);
public:
    InformacionPregunta(const InformacionPregunta&);
    InformacionPregunta();
    ~InformacionPregunta();
    InformacionPregunta & operator= (const InformacionPregunta &);
    void copia(const InformacionPregunta&);
    
    long int getNumTotalPal() const {
        return numTotalPal;
    }

    void setNumTotalPal(long int numTotalPal) {
        this->numTotalPal = numTotalPal;
    }

    long int getNumTotalPalDiferentes() const {
        return numTotalPalDiferentes;
    }

    void setNumTotalPalDiferentes(long int numTotalPalDiferentes) {
        this->numTotalPalDiferentes = numTotalPalDiferentes;
    }

    long int getNumTotalPalSinParada() const {
        return numTotalPalSinParada;
    }

    void setNumTotalPalSinParada(long int numTotalPalSinParada) {
        this->numTotalPalSinParada = numTotalPalSinParada;
    }

    
private:
    long int numTotalPal; //n� total de palabras en la pregunta
    long int numTotalPalSinParada; //n� total de palabras en la pregunta sin stop-words
    long int numTotalPalDiferentes; 
    /*n� total de palabras diferentes en la pregunta que no sean stop words
     * sin acumular la frecuencia de cada una de ellas
*/
};

#endif /* INDEXADORINFORMACION_H */

