/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   indexadorHash.cpp
 * Author: lourdes
 * 
 * Created on April 19, 2018, 11:12 AM
 */
#include <sys/types.h>
#include <sys/stat.h>
#include "indexadorHash.h"
typedef std::unordered_map<std::string, InformacionTermino> tipo_indice;
typedef std::unordered_map<std::string, InformacionTerminoPregunta> tipo_indicePregunta;
typedef std::unordered_map<std::string, InfDoc> tipo_indiceDocs;
typedef std::unordered_map<long int, InfTermDoc> tipo_ldocs;


void IndexadorHash::file_to_stopWords(const std::string& filename){
    ifstream i;
        string cadena = "";
        list<string> tokens;
        
        i.open(filename);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << filename << "\n";
            
        }else{
            
            while(getline(i, cadena)){
                
                
                stopWords.insert(cadena);

            }
        }
        i.close();
}
/*"fichStopwords" ser� el nombre del archivo que contendra todas las palabras de parada (una palabra 
     * por cada linea del fichero) y se almacenar� en el campo privado "ficheroStopWords". Asimismo,
     * almacenar� todas las palabras de parada que contenga el archivo en el campo privado "stopWords", 
     * el indice de palabras con parada.
     * "delimitadores" ser� el string que contiene todos los delimitadores utilizados por el tokenizador
     * (campo privado tok).
     * detectComp y minuscSinAcentos seran los parametros que se pasaran al tokenizador.
     * "dirIndice" sera el directorio del disco duro donde se almacenara el indice (campo privado "directorioIndice").
     * Si dirIndice="" entonces se almacenara en el directorio donde se ejecuta el programa.
     * tStemmer inicializara la variable privada "tipoStemmer":
     *      0 = no se aplica stemmer: se indexa el t�rmino tal y como aparece tokenizado
     *      1 = stemmer de porter para espa�ol
     *      2 = stemmer de porter para ingles
     * "almEnDisco" inicializara la variable privada "almacenarEnDisco"
     * "almPosTerm" inicializara la variable privada "almacenarPosTerm"
     * Los indices (p.ej. indice, indiceDocs e informacionColeccionDocs) quedaran vacios
     */
    IndexadorHash::IndexadorHash(const std::string& fichStopWords, const std::string& delimitadores, const bool& detectComp, 
            const bool& minuscSinAcentos, const std::string& dirIndice, const int& tStemmer, 
            const bool& almEnDisco, const bool& almPosTerm){
        ficheroStopWords = fichStopWords;
        file_to_stopWords(ficheroStopWords);
        tok = Tokenizador(delimitadores, detectComp, minuscSinAcentos);
        directorioIndice = dirIndice;
        tipoStemmer = tStemmer;
        almacenarEnDisco = almEnDisco;
        almacenarPosTerm = almPosTerm;
        idDocPos = 1;
        
    }
    
    /*Constructor para incializar IndexadorHash a partir e una indexacion previamente
     * realizada que habra sido almacenada en "directorioIndexacion" mediante el metodo 
     * "bool GuardarIndexacion()". Con ello toda la parte privada se inicializara convenientemente,
     * igual que si se acabase de indexar la coleccion de documentos. En caso que no exista 
     * el directorio o que no contenga los datos de la indexacion se tratara la excepcion correspondiente */
    IndexadorHash::IndexadorHash(const std::string& directorioIndexacion){
        //lee de lo que he guardado en GuardarIndexacion();
    }
    
    IndexadorHash::IndexadorHash(const IndexadorHash& ih){
        IndexadorHash::copia(ih);
    }
    void IndexadorHash::copia(const IndexadorHash& ih){
        almacenarEnDisco = ih.almacenarEnDisco;
        almacenarPosTerm = ih.almacenarPosTerm;
        directorioIndice = ih.directorioIndice;
        ficheroStopWords = ih.ficheroStopWords;
        indice = ih.indice;
        indiceDocs = ih.indiceDocs;
        indicePregunta = ih.indicePregunta;
        infPregunta = ih.infPregunta;
        informacionColeccionDocs = ih.informacionColeccionDocs;
        pregunta = ih.pregunta;
        stopWords = ih.stopWords;
        tipoStemmer = ih.tipoStemmer;
        tok = ih.tok;
        idDocPos = ih.idDocPos;
    }
    
    IndexadorHash::~IndexadorHash(){
        indiceDocs.clear();
        informacionColeccionDocs.~InfColeccionDocs();
        pregunta = "";
        indicePregunta.clear();
        infPregunta.~InformacionPregunta();
        stopWords.clear();
        ficheroStopWords = "";
        tok.~Tokenizador();
        directorioIndice = "";
        idDocPos = 0;
        
    }
    
    IndexadorHash& IndexadorHash::operator= (const IndexadorHash& ih){
        if(this != &ih){
            //(*this).~IndexadorHash();
            IndexadorHash::copia(ih);
        }
        return *this;
    }
    Fecha IndexadorHash::FechaModFichero(const std::string& file)const{
        struct stat stat_buf;
        stat(file.c_str(), &stat_buf);
        
        Fecha f;
        f.SetDate(time(&stat_buf.st_mtime));
        return f;
    }
    
    int IndexadorHash::CalculaTamFichero(const std::string& file)const{
        struct stat stat_buf;
        int rc = stat(file.c_str(), &stat_buf);
        return rc == 0 ? stat_buf.st_size : -1;
    }
    
    /*En el caso que aparezcan documentos repetidos o que ya estuviesen previamente 
     * indexados (ha de coincidir el nombre del documento y el directorio en que 
     * se encuentra),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran 
     * (borrar el documento previamente indexado e indexar el nuevo) en caso que
     * la fecha de modificacion del docuemnto sea mas reciente que la almacenada
     * previamente (class "InfDoc" campo "fechaModificacion").
     * Los caso de reindexacion mantendr�n el mismo idDoc
     */
    bool IndexadorHash::IndexarDocumento(const std::string& file){
        //primero buscar si file ya estaba (por hacer), almacenar su peso y cosas
        ifstream i;
        string cadena;
        
        i.open(file);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << file << "\n";
            return false;
        }else{
            //vamos a rellenar algunos datos del documento:

            InfDoc id;
            id.SetFechaModificacion(FechaModFichero(file));
            id.SetIdDoc(idDocPos);
            long int tamBytes = CalculaTamFichero(file);
            id.SetTamBytes(tamBytes);
            indiceDocs.insert({file,id});
            
            //vamos a rellenar algunos datos de informacionColeccionDocs
            informacionColeccionDocs.setNumDocs(informacionColeccionDocs.getNumDocs() +1);
            informacionColeccionDocs.setTamBytes(informacionColeccionDocs.getTamBytes() + tamBytes);
            
            //se usa mas adelante para actualizar el indiceDocs sin recalcular la pos del file
            tipo_indiceDocs::iterator docu = indiceDocs.find(file);
            
            
            std::list<string> tokens;
            int pos = 0;
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    //tokenizamos el contenido de cadena
                    tok.Tokenizar(cadena,tokens);
                    //vamos a indexar los tokens
                    
                    for (std::list<std::string>::const_iterator iterator = tokens.begin(), end = tokens.end(); iterator != end; ++iterator) {
                        //para cada palabra
                        //1� miramos si esta en stopwords
                        std::unordered_set<std::string>::const_iterator lwords = stopWords.find(*iterator);
                        //si no esta vamos a gestionarla  
                        if(lwords == stopWords.end()){
                            tipo_indice::iterator word = indice.find(*iterator);
                            if(word == indice.end()){
                                //la palabra no estaba ya almacenada


                                list<int> posTerm;
                                posTerm.push_back(pos);
                                //inftermdoc
                                InfTermDoc ifd;
                                ifd.setFt(1);
                                ifd.setPosTerm(posTerm);
                                //ldocs
                                tipo_ldocs l; 

                                l.insert({idDocPos,ifd});
                                //informaciontermino
                                InformacionTermino it;
                                it.setFtc(1);
                                it.setL_docs(l);

                                indice.insert({*iterator,it});
                                
                                //vamos a actualizar indiceDocs                                 
                                docu->second.SetNumPal(docu->second.GetNumPal() + 1);
                                docu->second.SetNumPalSinParada(docu->second.GetNumPalSinParada() + 1);
                                docu->second.SetNumPalDiferentes(docu->second.GetNumPalDiferentes() + 1);
                                
                                
                                //vamos a ocuparnos de infColeccionDocs
                                informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()+ 1);
                                informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() + 1);
                                informacionColeccionDocs.setNumTotalPalDiferentes(informacionColeccionDocs.getNumTotalPalDiferentes() + 1);
                             
                            }                   
                            else{
                                //la palabra ya estaba
                                
                                //vamos a actualizar el indice
                                word->second.setFtc(word->second.getFtc() + 1);
                
                                word->second.getL_docs().find(idDocPos)->second.setFt
                                                (word->second.getL_docs().find(idDocPos)->second.getFt() + 1);
                                list<int> lpos = word->second.getL_docs().find(idDocPos)->second.getPosTerm();
                                lpos.push_back(pos);
                                word->second.getL_docs().find(idDocPos)->second.setPosTerm(lpos);
                                
                                //vamos a actualizar indiceDocs
                                docu->second.SetNumPal(docu->second.GetNumPal() + 1);
                                docu->second.SetNumPalSinParada(docu->second.GetNumPalSinParada() + 1);
                                
                                //vamos a ocuparnos de infColeccionDocs
                                informacionColeccionDocs.setNumTotalPal(informacionColeccionDocs.getNumTotalPal()+ 1);
                                informacionColeccionDocs.setNumTotalPalSinParada(informacionColeccionDocs.getNumTotalPalSinParada() + 1);
                            }
                        } 
                        ++pos;
                    }
                }
            }
            //lo preparamos para que el siguiente doc que se lea tenga otro id
            ++ idDocPos ;
        }
        i.close();
        return true;
    
    }
    
    /*Devuelve true si consigue crear el indice para la coleccion de docuemntos
     * detallada en ficheroDocumentos, el cual contendra un monbre de documento
     * por linea. Los a�adira a los ya existentes anteriormente en el indice
     * Devuelve falso sino  finaliza la indexacion p.ej por falta de memoria, 
     * mostrando el mensaje de excepcion correspondiente, indicando el documento 
     * y termino en el que se ha quedado
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente 
     * indexados (ha de coincidir el nombre del documento y el directorio en que 
     * se encuentra),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran 
     * (borrar el documento previamente indexado e indexar el nuevo) en caso que
     * la fecha de modificacion del docuemnto sea mas reciente que la almacenada
     * previamente (class "InfDoc" campo "fechaModificacion").
     * Los caso de reindexacion mantendr�n el mismo idDoc
     */
    bool IndexadorHash::Indexar(const std::string& ficheroDocumentos){
        
        ifstream i;
        string cadena;
        
        i.open(ficheroDocumentos);
        if(!i){
            cerr << "ERROR: No existe el archivo: " << ficheroDocumentos << "\n";
            return false;
        }else{
            while(!i.eof()){
                cadena = "";
                getline(i, cadena);
                if(cadena.length()!= 0){
                    //indexamos el contenido de cadena
                    IndexarDocumento(cadena);
                }
            }
        }
        i.close();
        
        
    }
    
    /* Devuelve true si consigue crear el indice para la coleccion de documentos
     * que se encuentra en el directorio (y subdirectorios que contenga) dirAIndexar 
     * (independientemente de extension de los mismos). Se considerara que todos los
     * documentos del directorio seran ficheros de texto. Los a�adir� a los ya existentes
     * anteriormente en el indice.
     * Devuelve falso si no finaliza la indexacion p.ej por falta de memoria o porque no
     * exista dirAIndexar, mostrando el mensaje de error correspondiente, indicando el documento y 
     * t�rmino en el que se ha quedado.
     * En el caso que aparezcan documentos repetidos o que ya estuviesen previamente
     * indexados (ha de coincidir el nombre del documento y el directorio en el que se encuentre),
     * se mostrara el mensaje de excepcion correspondiente, y se re-indexaran (borrar el documento
     * previamente indexado e indexar de nuevo) en caso que la fecha de modificacion del documento 
     * sea mas reciente que la almacenada previamente (class "InfDoc" campo "fechaModificacion").
     * Los casos de reindexacion mantendran el mismo idDoc
     */
    //bool IndexarDirectorio(const std::string& dirAIndexar);
    
    /* Se guardara en disco duro (directorio contenido en la variable privada "directorioIndice")
     * la indexacion actualmente en memoria (incluidos todos los parametros de la parte privada).
     * La forma de almacenamiento la determinara el alumno. El objetivo es que esta indexacion
     * se pueda recuperar posteriormente mediante el constructor "IndexadorHash(const string&)".
     * Por ejemplo, supongamos que se ejecuta esta secuencia de comandos: 
     * "IndexadorHash a ("./fichStopWords.txt", "[ ,.", "./dirIndexPrueba", 0, false);
     * "a.Indexar("./fichConDocsAIndexar.txt"); a.GuardarIndexacion();"
     * Entonces mediante el comando: "IndexadorHash b("./dirIndexPrueba");" se recuperara la 
     * indexacion realizada en la secuencia anterior, cargandola en b.
     * Devuelve falso si no finaliza la operacion (p.ej por falta de moemoria o el nombre del directorio
     * contenido en directorioIndice no es correcto), mostrando el mensaje de error correspondiente.
     * en caso de que no existiese el directorio directorioIndice, habr�a que crearlo previamente
     * 
     */
    //bool GuardarIndexacion() const;
    
    /* Vacia la indexacion que tuviese en ese momento e inicializa IndexadorHash a partir
     * de una indexacion previamente realizada que habra sido almacenada en "directorioIndexacion"
     * mediante el metoo "bool GuardarIndexaxion();". Con ello toda la parte privada se inicializara
     * convenientemente, igual que si se acabase de indexar la coleccion de documentos.
     * En caso que no existiera el directorio o que no contengalos datos de la indexaci�n
     * se tratar� la excepcion correspondiente, y se devolvera false
     */
    //bool RecuperarIndexacion(const std::string& directorioIndexacion);
    
    //void IndexadorHash::ImprimirIndexacion()const{
    
    
    
    /*Devuelve true si consigue crear el indice para la pregunta "preg".
     * antes de realizar la indexacion vaciara los campos privados indicePregunta
     * e infPregunta. Generara la misma informacion que en la indexacion de documentos
     * pero dejandola toda accesible en memoria principal (mediante las variables privadas
     * "pregunta, indicePregunta, infPregunta")
     * Devuelve false si no finaliza la operacion (p.ej por falta de memoria o bien 
     * si la pregunta no contiene ningun termino con contenido), comstrando el mensaje
     * de error correspondiente
     */
    bool IndexadorHash::IndexarPregunta(const std::string& preg){
        bool aDevolver = false;
        indicePregunta.clear();
        infPregunta.~InformacionPregunta();
        std::list<std::string> lPreg;
        tok.Tokenizar(preg,lPreg);
        InformacionTerminoPregunta itp;
        int pos = 0;
        infPregunta.setNumTotalPal(lPreg.size());
        
        for(std::list<std::string>::iterator iterator = lPreg.begin(); iterator != lPreg.end();){

            std::unordered_set<std::string>::const_iterator lwords = stopWords.find(*iterator);
            if(lwords != stopWords.end()){
                iterator = lPreg.erase(iterator);
            }
            //else no es un stopwords vamos a almacenarla en indicepregunta
            else{
                aDevolver = true;
                tipo_indicePregunta::iterator ipreg = indicePregunta.find(*iterator);
                //si ya estaba la palabra una vez
                if(ipreg != indicePregunta.end()){
    
                    list<int> lpos = ipreg->second.getPosTerm();
                    lpos.push_back(pos);
                            
                     ipreg->second.setFt(ipreg->second.getFt() + 1);
                     ipreg->second.setPosTerm(lpos);
                     infPregunta.setNumTotalPalSinParada(infPregunta.getNumTotalPalSinParada() + 1); 
                    
                }else{
                    
                    itp.setFt(1);
                    list<int> lpos;
                    lpos.push_back(pos);
                    itp.setPosTerm(lpos);//meter la posicion del termino
                    indicePregunta.insert({{*iterator},{itp}});
                    infPregunta.setNumTotalPalSinParada(infPregunta.getNumTotalPalSinParada() + 1);
                    infPregunta.setNumTotalPalDiferentes(infPregunta.getNumTotalPalDiferentes() + 1);
                    
                }
                 ++ iterator;
            }
            ++pos;
        }    
        return aDevolver;
        
    }
    
    /*Devuelve true si hay una pregunta indexada (con al menos un termino que no sea
     * palabra de parada, o sea que haya algun termino indexado en indicePregunta), 
     * devolviendola en "preg"
     */
    bool IndexadorHash::DevuelvePregunta(std::string& preg) const{
        preg = "";
        
        for (tipo_indicePregunta::const_iterator iterator = indicePregunta.begin(); iterator != indicePregunta.end(); ++iterator) {
            preg += "\tId.Doc: " + iterator->first + " ";
        }
        
        return (preg != "");
    }
    
    /*Devuelve true si word esta indexado en la pregunta, devolviendo su informacion
     * almacenada "inf". En caso que no este devolveria devolveria "inf" vacio*/
    bool IndexadorHash::DevuelvePregunta(const std::string& word, InformacionTerminoPregunta& inf)const{
        //cambiar por find
        if (!indice.empty()){
            for (tipo_indicePregunta::const_iterator iterator = indicePregunta.begin(); iterator != indicePregunta.end(); ++iterator) {
                if (word == iterator->first){
                    inf = iterator->second;
                    return true;
                }
            } 
        }
        return false;  
        
    }
    
    /*Devuelve true si hay una pregunta indexada, devolviendo su informacion almacenada
     * (campo privado "infPregunta")en "inf". En caso que no este, devolveria "inf" vacio
     */
    bool IndexadorHash::DevuelvePregunta(InformacionPregunta& inf)const{
        if (!indicePregunta.empty()){
            inf = infPregunta;
            return true;
        }
        return false;
    }
    
    void IndexadorHash::ImprimirIndexacionPregunta()const{
        cout << "Pregunta indexada: " << pregunta << "\n";
        cout << "Terminos indexados en la pregunta: " << "\n";
        for (tipo_indicePregunta::const_iterator iterator = indicePregunta.begin(); iterator != indicePregunta.end(); ++iterator) {
                cout << iterator->first << "\t" << iterator->second << "\n";
            } 

        cout << "Informacion de la pregunta: : " << infPregunta << "\n";
    }
    
    void IndexadorHash::ImprimirPregunta()const{
        cout << "Pregunta indexada: " << pregunta << "\n";
        cout << "Informacion de la pregunta: " << infPregunta << "\n";
    }
    
    /*Devuelve true si wor esta indexado, devolviendo su informacion almacenada "inf".
     * En caso que no este, devolveria "inf" vacio
     */
    bool IndexadorHash::Devuelve(const std::string& word, InformacionTermino& inf)const{
        //cambiar por find
        if (!indice.empty()){
            for (tipo_indice::const_iterator iterator = indice.begin(); iterator != indice.end(); ++iterator) {
                if (word == iterator->first){
                    inf = iterator->second;
                    return true;
                }
            } 
        }
        return false;
         
    }
    
    /*Devuelve true si word esta indexado y aparece en el documento de nobre nomDoc, 
     * en cuyo caso devuelve la inormacion alamacenada para word en el documento.
     * En caso que noeste, devolveria "InfDoc" vacio
     */
    bool IndexadorHash::Devuelve(const std::string& word, const std::string& nomDoc, InfTermDoc& InfDoc)const{
        
        tipo_indice::const_iterator iterator = indice.find(word); 
        if (iterator != indice.end()){
            tipo_indiceDocs::const_iterator iterator2 = indiceDocs.find(nomDoc);
            if(iterator2 != indiceDocs.end()){
                //el id
                //iterator2->second.GetIdDoc();
                tipo_ldocs::const_iterator iterator3 = iterator->second.getL_docs().find(iterator2->second.GetIdDoc());
                if(iterator3 != iterator->second.getL_docs().end()){
                    InfDoc = iterator3->second;
                    return true;
                }
            }
        }
        
        return false;
    }
    
    //Devuelve true si word aparece como termino indexado
    bool IndexadorHash::Existe(const std::string& word)const{
        tipo_indice::const_iterator iterator = indice.find(word); 
        return (iterator != indice.end());
    }
    
    //Devuelve true si se realiza el borrado p.ej si word aparece como termino indexado
    bool IndexadorHash::Borra(const std::string& word){
        
        if (indice.erase(word) == 1){
            return true;
        }
        return false;
    }
    
    /*Devuelve true si nomDoc esta indexado y se realiza el borrado de todos los 
     * terminos del documento y del docuemnto en los campos privados "indiceDocs" e
     * "informacionColeccionDocs"
     */
    bool IndexadorHash::BorraDoc(const std::string& nomDoc){
        
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc);
        if(iterator != indiceDocs.end()){
            informacionColeccionDocs.setNumDocs
                        (informacionColeccionDocs.getNumDocs() - 1);
            informacionColeccionDocs.setNumTotalPal
                        (informacionColeccionDocs.getNumTotalPal()- iterator->second.GetNumPal() );
            informacionColeccionDocs.setNumTotalPalSinParada
                        (informacionColeccionDocs.getNumTotalPalSinParada() - iterator->second.GetNumPalSinParada() );
            informacionColeccionDocs.setNumTotalPalDiferentes
                        (informacionColeccionDocs.getNumTotalPalDiferentes() - iterator->second.GetNumPalDiferentes() );
            informacionColeccionDocs.setTamBytes
                        (informacionColeccionDocs.getTamBytes() - iterator->second.GetTamBytes());
            
            for (std::unordered_map<string, InformacionTermino>::const_iterator iteratorind = indice.begin(), end = indice.end(); iteratorind != end; ++iteratorind) {
                tipo_ldocs::const_iterator iterator2 = iteratorind->second.getL_docs().find(iterator->second.GetIdDoc());
                if(iterator2 != iteratorind->second.getL_docs().end()){
                    iteratorind->second.getL_docs().erase(iterator2);
                }
            }
            
            
            
            indiceDocs.erase(iterator);
            return true;

        }
        return false;
    }
    
    //Borra todos los terminos del indice de documento
    void IndexadorHash::VaciarIndiceDocs(){
        indiceDocs.clear();
    }
    
    //Borra todos los terminos del indice de la pregunta
    void IndexadorHash::VaciarIndicePreg(){
        indicePregunta.clear();
    }
    
    //sera true si word esta indexado, sustituyendo la informacion almacenada por "inf"
    bool IndexadorHash::Actualiza(const std::string& word, const InformacionTermino& inf){
        tipo_indice::iterator iterator = indice.find(word); 
        if (iterator != indice.end()){
            iterator->second = inf;

            return true;
        }
        return false;
    }
    
    //Sera true si se realiza la insercion p.ej si word no estaba previamente indexado
    bool IndexadorHash::Inserta(const std::string& word, const InformacionTermino& inf){
        tipo_indice::const_iterator iterator = indice.find(word); 
        if (iterator == indice.end()){
            indice.insert({{word}, {inf}});
            

            return true;
        }
        return false;
    }
    
    //Devolvera el numero de terminos diferentes indexados (cardinalidad de campo privado "indice")
    int IndexadorHash::NumPalIndexadas()const{
        return indice.size();
    }
    
    //Devuelve el contenido del campo privado "ficheroStopWords"
    std::string IndexadorHash::DevolverFichPalParada()const{
        return ficheroStopWords;
    }
    
    /*Mostrara por pantalla las paralbras de paraa almacenadas (originales, sin aplicar stemming):
     * una plabra por linea (salto de linea al final de cada palabra)
     */
    void IndexadorHash::ListarPalParada()const{
        for (std::unordered_set<string>::const_iterator iterator = stopWords.begin(), end = stopWords.end(); iterator != end; ++iterator) {
                cout  << *iterator<< "\n";
        }
    }
    
    //Devolvera el numero de palabras de parada almacenadas
    int IndexadorHash::NumPalParada() const{
        return stopWords.size();
    }
    
    //Devuelve los delimitadores utilizados por el tokenizador
    std::string IndexadorHash::DevolverDelimitadores() const{
        return tok.getDelimiters();
    }
    
    //Devuelve si el tokenizador analiza los casos especiales
    bool IndexadorHash::DevolverCasosEspeciales () const{
        return tok.isCasosEspeciales();
    }
    
    //Devuelve si el tokenizador pasa a minusculas y sin acentos
    bool IndexadorHash::DevolverPasarAminuscSinAcentos () const{
        return tok.isPasarAminuscSinAcentos();
    }
    
    //Devuelve el valor de almacenarPosTerm
    bool IndexadorHash::DevolverAlmacenarPosTerm() const{
        return almacenarPosTerm;
    }
    
    //Devuelve "directorioIndice" (el directorio del disco duro donde se almacenara el indice)
    std::string IndexadorHash::DevolverDirIndice() const{
        return directorioIndice;
    }
    
    //Devolvera el tipo de stemming realizado en la indexcion de acuerdo con el valo de "sipoStemmer"
    int IndexadorHash::DevolverTipoStemming() const{
        return tipoStemmer;
    }
    
    //Devolvera el valor indicado en la variable privada "almEnDisco"
    bool IndexadorHash::DevolverAlmEnDisco() const{
        return almacenarEnDisco;
    }
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indice": */
    void IndexadorHash::ListarInfColeccDocs() const{
        cout << informacionColeccionDocs<< "\n";
    }
    
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla todos 
     * los terminos indexados del documento con nombre 
     * "nomDoc": cout << termino << '\t' << InformacionTermino << endl;
     * si no existe no se muestra nada*/
    bool IndexadorHash::ListarTerminos(const std::string& nomDoc)const{
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc); 
        if (iterator != indiceDocs.end()){
            for (tipo_indice::const_iterator terminosind = indice.begin(); terminosind != indice.end(); ++terminosind) {
                tipo_ldocs::const_iterator iterator2 = terminosind->second.getL_docs().find(iterator->second.GetIdDoc());
                if(iterator2 != terminosind->second.getL_docs().end() ){
                    cout << terminosind->first << '\t' << terminosind->second << "\n";
                }
            }
            return true;
         }
        return false;
    }
    
    /*Mostrar por pantalla el contenido del campo privado 
     * "indiceDocs": cout<< nomDoc << '\t' << infDoc << endl;
     */
    void IndexadorHash::ListarDocs() const{
        for (tipo_indiceDocs::const_iterator iterator = indiceDocs.begin(); iterator != indiceDocs.end(); ++iterator) {
            cout<< iterator->first << '\t' << iterator->second << "\n";
        }
    }
   
    /*Devuelve true si nomDoc existe en la coleccion y muestra por pantalla el contenido
     * del campo privado indiceDocs para el docuemnto con nombre 
     * "nomDoc": cout << nomDoc << '\t' << infDoc << endl;
     * si no existe no se muestra nada*/
    bool IndexadorHash::ListarDocs(const std::string& nomDoc) const{
        tipo_indiceDocs::const_iterator iterator = indiceDocs.find(nomDoc);
        if(iterator != indiceDocs.end()){
            cout << nomDoc << '\t' << iterator->second << "\n";
            return true;
        }
        return false;
    
    }